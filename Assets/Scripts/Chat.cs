using UnityEngine;
using System.Collections;
using System.Text;

public class Chat : MonoBehaviour {
	public string chatLine;
	public StringBuilder chatText;
	
	void Start() {
		chatText = new StringBuilder();
	}
	
	void OnGUI() {
		if(Network.isClient || Network.isServer) {
			ChatGUI();
		}
	}
	
	void ChatGUI() {
		GUILayout.BeginVertical( GUILayout.Width( 150 ) );
		
		chatLine = GUILayout.TextField( chatLine );
		if( (Input.GetKeyDown(KeyCode.Insert) || GUILayout.Button( "Send" )) && chatLine != "") {
			networkView.RPC( "ChatRPC", RPCMode.All, chatLine );
			chatLine = "";
		}
		
		GUILayout.TextArea( chatText.ToString());
		
		GUILayout.EndVertical();
	}
	
	[RPC]
	void ChatRPC( string line ) {
		chatText.AppendLine( line );
	}
}
