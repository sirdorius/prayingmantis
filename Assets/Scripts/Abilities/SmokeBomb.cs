using System.Collections;
using UnityEngine;

public class SmokeBomb : Skills {
	
	private ParticleSystem particle;
	private float time;
	
	// Use this for initialization
	void Start () {
		particle = GetComponent<ParticleSystem>();
		time = particle.duration + particle.startDelay;
		
		if (networkView.isMine)
			StartCoroutine("DelayedDestroy");
	}
	
	IEnumerator DelayedDestroy() {
		yield return new WaitForSeconds(time);
		Network.Destroy(gameObject);
	}
	
	override public void activate(Character c) {
		Vector3 newPosition = new Vector3(c.transform.position.x, 0, c.transform.position.z);
		Network.Instantiate(gameObject, newPosition, transform.rotation, 0);

		c.startAnimation ("Smoke");
	}
	
}

