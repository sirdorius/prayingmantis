using UnityEngine;
using System.Collections;

public class AbilityClone : Skills {
	override public void activate(Character c) {
		Vector2 d = c.getDirection();
		Vector2 deltaPos = -d/d.magnitude;

		Vector3 pos = c.transform.position+ (Vector3.right*deltaPos.x + Vector3.forward*deltaPos.y)*2f;
		GameObject clone = Network.Instantiate(gameObject, pos, c.transform.rotation, 0) as GameObject;

		NPC ninjaclone = clone.GetComponent<NPC>();
		Transform username = c.transform.Find ("username");
		GameObject copy = Instantiate (username.gameObject) as GameObject;
		Vector3 localPosition = copy.transform.localPosition;
		copy.transform.parent = ninjaclone.transform;
		copy.transform.localPosition = localPosition;

		ninjaclone.setVisible(false);
		ninjaclone.setInput(deltaPos, c.getDirection(), 0);
	}

	void OnCollisionEnter (Collision coll) {
		if(networkView.isMine) {
			Network.Destroy(gameObject);
		}
	}
}
