﻿using UnityEngine;
using System.Collections;

public class AbilityManager : MonoBehaviour {
	public Skills[] skills;
	public float abilitiesCooldown = 3f;
	private bool enabledAbilities = true;
	private float elapsedCooldownTime;
	private Cooldown HUDcooldown;

	private Character c;

	void Start() {
		c = GetComponent<Character> ();
	}

	public void OnConnect() {
		HUDcooldown = GameObject.FindObjectOfType(typeof(Cooldown)) as Cooldown;
		HUDcooldown.setCooldown(abilitiesCooldown);
	}

	// Update is called once per frame
	void Update () {
		if (! networkView.isMine) return; 

		if (Input.GetKeyDown(KeyCode.Alpha1)) {
			activate(0);
		}
		
		if (Input.GetKeyDown(KeyCode.Alpha2)) {
			activate(1);
		}
		
		if (Input.GetKeyDown(KeyCode.Alpha3)) {
			activate(2);
		}
	}

	private void activate(int n) {
		if(enabledAbilities) {
			skills[n].activate(c);
			enabledAbilities = false;
			elapsedCooldownTime = 0;
			StartCoroutine("stopAbilities");
		}
	}

	IEnumerator stopAbilities() {
		while(elapsedCooldownTime<abilitiesCooldown) {
			elapsedCooldownTime += Time.deltaTime;
			if(HUDcooldown!=null) HUDcooldown.cooltimeUpdate(elapsedCooldownTime);
			yield return null;
		}
		enabledAbilities = true;
	}
}
