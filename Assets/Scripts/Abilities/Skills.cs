using System.Collections;
using UnityEngine;

public abstract class Skills : MonoBehaviour {
	private Animator anim;
	private Character character;

	public void setCharacter(Character c) {
		character = c;
		anim = character.getAnimator();
	}

	protected IEnumerator stopAnim(string name){
		yield return new WaitForSeconds(anim.GetNextAnimatorStateInfo(0).length);
		anim.SetBool(name, false);
	}
	
	
	abstract public void activate(Character c);

}

