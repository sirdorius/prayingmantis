using UnityEngine;
using System.Collections;
using Holoville.HOTween;


public class NPC : Character {
    SoldierAI ai;
	
	override protected void Update() {
		float rot = Mathf.Rad2Deg * Mathf.Atan2 (-direction.y, direction.x);
		HOTween.To(transform, timeToLookAtCursor, new TweenParms().Prop("rotation", new Vector3(0,rot+90,180)));
	}

	override protected void ControlPolicy() {
	}

	override protected void Start() {
        ai = GetComponent<SoldierAI>();
	}

    override public void AfterRespawn() {
        base.AfterRespawn();
        ai.newPath();
    }
}
