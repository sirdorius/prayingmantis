using UnityEngine;
using System.Collections;

public class Shuriken : Skills {
	
	public float bulletSpeed;
	public float range = 10f;
	private GameObject shuriken;
	public float stunTime;
	private Vector3 initialPos;
	private int layerMask;

	// Use this for initialization
	void Start () {
		initialPos = transform.position;
		layerMask = 1<<LayerMask.NameToLayer("Ninja") | 1<<LayerMask.NameToLayer("Wall") |
			1<<LayerMask.NameToLayer("Soldier");
	}
	
	// Update is called once per frame
	void Update () {
	
		float amtToMove = bulletSpeed * Time.deltaTime;
		//transform.Rotate(Vector3.left * 50);
		if (amtToMove == 0) return;
		
		Vector3 oldPos = transform.position;
		transform.Translate(Vector3.left * amtToMove);
		Vector3 translationVector = transform.position - oldPos;
		RaycastHit hit = new RaycastHit();
		if (Physics.Raycast(oldPos, translationVector, out hit, translationVector.magnitude, layerMask) ) {
			transform.position = hit.point;
			destroyBullet(hit.collider);
		}
		else if((initialPos-transform.position).magnitude>range)
			destroyBullet(null);
		
	}
	
	void destroyBullet(Collider other) {
		if(true) {
			if (other != null) {
				Character charScript = other.GetComponent<Character>();
				if (charScript != null) {
					//other.transform.FindChild("HitBlood").particleSystem.Emit (10);
					charScript.stunChar(stunTime);
				}
			}
			renderer.enabled = false;
			bulletSpeed = 0;
			StartCoroutine(timedNetworkDestroy(0.5f));
		}
	}
	
	IEnumerator timedNetworkDestroy(float t) {
		renderer.enabled = false;
		bulletSpeed = 0;
		yield return new WaitForSeconds(t);
		Network.Destroy(gameObject);
	}
	
	override public void activate(Character c) {
		Vector2 d = c.getDirection();
		Vector2 deltaPos = -d/d.magnitude;

		Vector3 pos = c.transform.position+ (Vector3.right*deltaPos.x + Vector3.forward*deltaPos.y)*2f;
		Network.Instantiate(gameObject, pos,
			Quaternion.Euler(c.transform.rotation.eulerAngles+Vector3.up*90), 0);

		c.startAnimation ("Shuriken");
	}
	
	/*void OnTriggerEnter(Collider other) {
		collider.enabled = false;
		renderer.enabled = false;
		shurikenSpeed = 0;
		Network.Destroy(gameObject);
		var charScript = other.GetComponent<Character>();
		if (charScript != null) {
			print("Stunt");
			charScript.stunChar(stunTime);
		}
	}*/
	
}
