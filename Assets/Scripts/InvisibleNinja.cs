﻿using UnityEngine;
using System.Collections;

public class InvisibleNinja : MonoBehaviour {
	void Start() {
		if(Player.isNinja) {
			renderer.enabled = false;
		}
	}
}
