using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
	public float bulletSpeed;
	public float range = 10f;
	public int damage = 40;
	
	private Vector3 initialPos;
	private int layerMask;
	
	// Use this for initialization
	void Start () {
		if(networkView.isMine)
			initialPos = transform.position;
		layerMask = 1<<LayerMask.NameToLayer("Ninja") | 1<<LayerMask.NameToLayer("Wall") |
			1<<LayerMask.NameToLayer("Soldier");
	}
	
	
	void FixedUpdate () {
		if (bulletSpeed == 0) return;
		if(networkView.isMine) {
			float amtToMove = bulletSpeed * Time.deltaTime;
			if (amtToMove == 0) return;
			
			Vector3 oldPos = transform.position;
			transform.Translate(Vector3.forward * amtToMove);
			Vector3 translationVector = transform.position - oldPos;
			RaycastHit hit = new RaycastHit();
			if (Physics.Raycast(oldPos, translationVector, out hit, translationVector.magnitude, layerMask) ) {
				transform.position = hit.point;
				destroyBullet(hit.collider);
			}
			else if((initialPos-transform.position).magnitude>range)
				destroyBullet(null);
		}
	}
	
	void destroyBullet(Collider other) {
		if(Network.connections.Length<2 || networkView.isMine) {
			if (other != null) {
				Character charScript = other.GetComponent<Character>();
				if (charScript != null) {
					//Debug.LogError(name+" "+other.networkView.owner+" "+other.name);
                    if (Network.connections.Length > 1)
                        other.networkView.RPC("getDamage", RPCMode.Others, damage);
                    else
                        charScript.getDamage(damage);
				}
			}
			renderer.enabled = false;
			bulletSpeed = 0;
			StartCoroutine(timedNetworkDestroy(0.5f));
		}
	}
	
	IEnumerator timedNetworkDestroy(float t) {
		renderer.enabled = false;
		GetComponent<Bullet>().bulletSpeed = 0;
		yield return new WaitForSeconds(t);
		Network.Destroy(gameObject);
	}
}
