using UnityEngine;
using System.Collections;

public class NinjaProximity : MonoBehaviour {
	void OnTriggerEnter(Collider other) {
		var c = other.GetComponent<Character>();
		if (c) {
			c.killed();
			transform.parent.GetComponent<Animator>().SetBool("Hit", true);
			StartCoroutine(transform.parent.GetComponent<Character>().stopAnim("Hit"));
		}
		else if (other.gameObject.tag == "Turret") {
			Network.Destroy(other.transform.parent.gameObject);
			transform.parent.GetComponent<Animator>().SetBool("Hit", true);
			StartCoroutine(transform.parent.GetComponent<Character>().stopAnim("Hit"));
		}
		else if (other.gameObject.layer == LayerMask.NameToLayer("LowObstacle")) {
			transform.parent.GetComponent<Animator>().SetBool("Jump", true);
			StartCoroutine(transform.parent.GetComponent<Character>().stopAnim("Jump"));
		}
	}
}
