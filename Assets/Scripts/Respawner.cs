using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Respawner : MonoBehaviour {
	public Respawn [] respawns;
    private static Respawner instance;
    private List<Character> characters;

	// Use this for initialization
	void Awake () {
        instance = this;
        instance.characters = new List<Character>();
		Object[] o = GameObject.FindGameObjectsWithTag("Respawn");
		instance.respawns = new Respawn[o.Length];
		for(int i = 0; i<respawns.Length;i++)
			respawns[i] = ((GameObject)o[i]).GetComponent<Respawn>();
	}

    void Update() {
        if (instance.characters.Count < 1) return;
        Character c = characters[0];

        ArrayList indexes = new ArrayList();
        for (int i = 0; i < instance.respawns.Length; i++) {
            indexes.Add(i);
        }

        Respawn r;
        do {
            int index = (int)indexes[Random.Range(0, indexes.Count)];
            indexes.Remove(index);

            r = instance.respawns[index];
            if (!r.isOccupied()) {
                c.transform.position = new Vector3(r.transform.position.x, 0, r.transform.position.z);
                c.AfterRespawn();
                instance.characters.Remove(c);
                return;
            }
        } while (indexes.Count > 0);
    }
	
	public static void respawn(Character c) {
        instance.characters.Add(c);
	}
}
