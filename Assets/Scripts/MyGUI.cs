using UnityEngine;
using System.Collections;

public class MyGUI : MonoBehaviour {
	public GUIText healthText;
	public GUIText otherText;

	// Use this for initialization
	void Start() {
		setVisible(false);
	}
	
	public void setHealth(int v) {
		healthText.text = ""+v;
	}
	
	public void setOtherInfo(string v) {
		otherText.text = v;
	}
	
	public void setVisible(bool v) {
		guiTexture.enabled = v;
	}
	
	public void setNinjaHud(bool val) {
		transform.Find("objectivesIcon").GetComponent<GUITexture>().enabled = val;
		transform.Find("ammoIcon").GetComponent<GUITexture>().enabled = !val;
	}
}
