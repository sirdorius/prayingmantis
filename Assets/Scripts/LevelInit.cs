using UnityEngine;
using System.Collections;

public class LevelInit : MonoBehaviour {
	
	public float levelRotationAngle = -45;

	// Use this for initialization
	void Start () {
		transform.Rotate(0,levelRotationAngle,0);
	}
}
