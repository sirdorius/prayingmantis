using UnityEngine;
using System.Collections;

public class Sword : MonoBehaviour {
	public GameObject owner;
	
	void OnTriggerEnter (Collider other) {

		if (other.tag == "Soldier" &&
			other.networkView.isMine &&
			owner.animation.isPlaying
			) {
			other.GetComponent<Character>().killed();
		}
	}
}
