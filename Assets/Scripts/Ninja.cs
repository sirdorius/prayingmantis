using UnityEngine;
using System.Collections;

public class Ninja : Character {
	private WinningCondition wincond;

	protected override void Start (){
		wincond = GameObject.Find("Level").GetComponent<WinningCondition>();
		base.Start ();
	}
	
	public override void OnConnect() {
		base.OnConnect();
		HUD.setOtherInfo("" + wincond.getObjectiveCount() + "/" + wincond.totalObjectives);
		HUD.setNinjaHud(true);
	}
	
	// Update is called once per frame
	override protected void Update () {
		base.Update();
	}
	
	override protected void ControlPolicy () {
		base.ControlPolicy();
		
	}
	
	[RPC]
	void shoot() {
		networkView.RPC("animSword",RPCMode.All);
	}
	
	[RPC]
	void animSword() {
		animation.Play();
	}
	
	override public void killed() {
		Network.Destroy(gameObject);
	}

	public float timeToTakeObjective = 1f;
	private float timeEnter;
	
	void OnTriggerEnter(Collider other) {
		if(other.tag == "Objective") {
			timeEnter = 0;
		}
	}
	
	void OnTriggerStay(Collider other) {
		if(other.tag == "Objective") {
			timeEnter += Time.deltaTime*Time.timeScale;
			if(timeEnter >= timeToTakeObjective) {
				Destroy(other.gameObject);
				wincond.ObjectCaught();

                Objective o = other.GetComponent<Objective>();
                o.taken();

				if (networkView.isMine)
					HUD.setOtherInfo("" + wincond.getObjectiveCount() + "/" + wincond.totalObjectives);
			}
		}
	}
	
	void OnTriggerExit(Collider other) {
		if(other.tag == "Objective") {
			timeEnter = -1f;
		}
	}
	
	void OnDestroy() {
		Time.timeScale = 0;
		if (wincond) wincond.soldiersWin ();
		else Debug.LogWarning ("Ninja winconditon has a null reference");
	}
}
