using UnityEngine;
using System.Collections;

public class DebugScript : MonoBehaviour {
	
	
	
	public string serverIP = "localhost";
	public int serverPort = 345678;
	public int maxConnections = 3;
	
	public GameObject playerPrefab;

	void Awake() {
		Network.InitializeServer(maxConnections, serverPort, false);
		transform.Rotate(new Vector3(0,-20,0));
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.G)) {
			Network.Instantiate(Resources.Load("Prefabs/Soldier"), Vector3.zero, Quaternion.identity, 0);
		}
		if (Input.GetKeyDown(KeyCode.N)) {
			Network.Instantiate(Resources.Load("Prefabs/Ninja"), Vector3.zero, Quaternion.identity, 0);
		}
		if (Input.GetKeyDown(KeyCode.I)) {
			Network.Instantiate(playerPrefab, Vector3.zero, Quaternion.identity, 0);
		}
	}
}
