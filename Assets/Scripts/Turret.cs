using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class Turret : MonoBehaviour,NinjaDiscoverObserver {
	public Weapon weapon;
	public VisionCone visionCone;
	public float speed = 1f;
	public NinjaDiscover ninjaDiscover;


	void Awake() {
        // HOTween.Init (false, false, true);
		// HOTween.EnableOverwriteManager ();
	}
	void Start() {
		ninjaDiscover.AddObserver (this);
	}
	
	void Update() {
		if (! networkView.isMine) return;

		if(weapon.bulletsRemaning()==0) {
				weapon.reload();
		}
	}

	public void OnDiscoverEnter(Character c) {
	}

	public void OnDiscoverStay(Character c) {
		// TODO: la torretta si inclina verso il basso
		Vector3 dir = Vector3.right * c.transform.position.x +
			Vector3.forward * c.transform.position.z
				- Vector3.right * weapon.transform.position.x
				- Vector3.forward * weapon.transform.position.z;
		dir.Normalize();
		
		// weapon.transform.LookAt(ninja.transform.position);
		
		Quaternion needRotation = Quaternion.LookRotation(dir);
		weapon.transform.rotation = Quaternion.Slerp(weapon.transform.rotation,
		                                             needRotation,
		                                             speed*Time.deltaTime); 
		
		weapon.shoot();
	}
	public void OnDiscoverExit(Character c) {
	}
}
