using UnityEngine;
using System.Collections;

public class TrailerCamera : MonoBehaviour {
	public Transform player;
	public Vector3 offset;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Camera.main.gameObject.transform.LookAt(player.transform);
	}
}
