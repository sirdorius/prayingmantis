﻿using UnityEngine;
using System.Collections;

public class AddScript : MonoBehaviour {
	public MonoBehaviour script;
	public string[] ColliderTags;

	// Use this for initialization
	void Start() {
		script.enabled = false;
	}

	void OnCollisionEnter(Collision collider) {
		foreach(string tag in ColliderTags) {
			if(collider.gameObject.tag == tag) {
				script.transform.parent = collider.transform;
				script.enabled = true;
				Destroy(gameObject);

				return;
			}
		}
	}
}
