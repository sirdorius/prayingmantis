using UnityEngine;
using System.Collections;

public class Respawn : MonoBehaviour {
	private bool occupied;

	void OnTriggerEnter(Collider c) {
		occupied = true;
	}
	void OnTriggerExit(Collider c) {
		occupied = false;
	}
	
	public bool isOccupied() {
		return occupied;
	}
}
