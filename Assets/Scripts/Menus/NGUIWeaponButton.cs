using UnityEngine;
using System.Collections;

public class NGUIWeaponButton : MonoBehaviour {
	public NGUIOnlineMenu onlinemenu;
	public MatchInitialization matchInitialization;
	public MatchInitialization.Weapons weapon;
	
	public UILabel playerName;

	// Use this for initialization
	void OnClick() {
		
		matchInitialization.setChoice("Soldier", playerName.text);
		matchInitialization.setWeapon(weapon);
		Application.LoadLevel(onlinemenu.GameScene);
	}
}
