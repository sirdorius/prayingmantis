using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {
	public MenuState initialState;
	private MenuState stateSelected;
	
	public GUIStyle backgroundStyle;
	public GUIStyle buttonStyle;
	public GUIStyle fieldStyle;
	
	void Start() {
		stateSelected = initialState;
	}
	
	void OnGUI () {
		GUILayout.BeginArea( new Rect(0,0, Screen.width, Screen.height), backgroundStyle);
		stateSelected.OnGui();
		GUILayout.EndArea();
	}
	
	public void setState (MenuState state) {
		stateSelected = state;
	}
}
