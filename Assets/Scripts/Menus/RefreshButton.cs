using UnityEngine;
using System.Collections;

public class RefreshButton : MonoBehaviour {
	public OnlineMenu onlinemenu;

	public void OnClick() {
		onlinemenu.Refresh();
	}
}
