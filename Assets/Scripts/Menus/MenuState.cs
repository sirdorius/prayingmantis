using UnityEngine;
using System.Collections;

public abstract class MenuState : MonoBehaviour {
	public Menu menu;
	
	abstract public void OnGui();
	
	protected int getOffset(int lenght, int dim, int num) {
		return (lenght-dim*num)/(num+1);
	}
}
