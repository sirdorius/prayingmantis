using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class MenuInit : MonoBehaviour {

	// Use this for initialization
	void Awake () {
		HOTween.Init(false, false, true);
		HOTween.EnableOverwriteManager(false);
	}
}
