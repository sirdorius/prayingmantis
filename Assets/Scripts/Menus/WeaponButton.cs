using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WeaponButton : MonoBehaviour {
	public OnlineMenu onlinemenu;
	public MatchInitialization matchInitialization;
	public MatchInitialization.Weapons weapon;
	
	public Text playerName;

	// Use this for initialization
	public void OnClick() {
		
		matchInitialization.setChoice("Soldier", playerName.text);
		matchInitialization.setWeapon(weapon);
		Application.LoadLevel(onlinemenu.GameScene);
	}
}
