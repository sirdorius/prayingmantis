using UnityEngine;
using System.Collections;

public class NGUIRefreshButton : MonoBehaviour {
	public NGUIOnlineMenu onlinemenu;

	public void OnClick() {
		onlinemenu.Refresh();
	}
}
