using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class MatchInitialization : MonoBehaviour {
	public GameObject ninjaPrefab;
	public GameObject soldierPrefab;
	public Vector3 ninjaSpawn;
	private string choice;
	
	public int playersNeeded = 4;
	
	public enum Weapons {RifleAssalt,Shotgun};
	private Weapons weapon = Weapons.RifleAssalt;
	
	private Character character;
	private GameTimer gametimer;
	
	private string playerName;
	
	// Use this for initialization
	void Start () {
		DontDestroyOnLoad(gameObject);
	}
	
	
	
	void OnLevelWasLoaded () {
		/*
		 * Qualcosa mette timeScale a 0
		 * cio' provoca il malfunzionamento di hotween
		 * 
		 * ho provato a controllare se e' colpa di WinningCondition, ci andava comunque
		 */
		Time.timeScale = 1;

		/*
	 	* Non deve essere eseguito se carica il menu o un livello che non e' una mappa
	 	*/
	 	if(Application.loadedLevel == 0) return;

		if(choice == "Ninja") {
			GameObject c = Network.Instantiate(ninjaPrefab, ninjaSpawn, Quaternion.identity, 0) as GameObject;
			character = c.GetComponent<Character>();
			Player.instance = character;

			gametimer = GameObject.FindObjectOfType(typeof(GameTimer)) as GameTimer;
			gametimer.enabled = false;
		}
		else if(choice == "Soldier") {
			Network.isMessageQueueRunning = true;
			GameObject c = Network.Instantiate(soldierPrefab,
				transform.position,
				Quaternion.identity, 0) as GameObject;
			
			character = c.GetComponent<Character>();
			Player.instance = character;

            Respawner.respawn(character);

			Weapon w = c.GetComponentInChildren<Weapon>();
			
			if(weapon == Weapons.Shotgun) {
				w.bullets = 36;
				w.damage = 20;
				w.bulletsInATime = 6;
				w.timeBetweenShoots = 0.3f;
				w.reloadTime = 1f;
				w.deltaAccuracyCone = 15f;
				w.bulletSpeed = 100f;
				w.range = 2f;
				w.bulletSpeed = 20f;
			}
		}
		else {
			print (choice);
			print("something went terribly wrong");
		}
		
		//character.setName(playerName);
		character.networkView.RPC("setName", RPCMode.AllBuffered, playerName);
	}
	
	void OnGUI() {
		int width = 100, height = 100;
		if(Application.loadedLevel == 1) {
			Character[] p = GameObject.FindObjectsOfType(typeof(Character)) as Character[];
			int players = p.Length;
			if(players>=playersNeeded) {
				StartGame();
			}
			else {
				character.enabled = false;
				GUI.Label(new Rect((Screen.width-width)/2 ,(Screen.height-height)/3*2,width,height),
					"Waiting for players "+players+"/"+playersNeeded);
			}
		}
	}
	
	void StartGame () {
		if(choice == "Ninja")
			gametimer.enabled = true;
		character.enabled = true;
		Destroy(gameObject);
	}
	
	public void setChoice(string c, string name) {
		playerName = name;
		choice = c;
	}
	
	public void setWeapon(Weapons w) {
		weapon = w;
	}
}
