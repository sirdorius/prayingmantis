using UnityEngine;
using System.Collections;

public class NGUIOnlineMenu : MonoBehaviour {
	public string gameName = "PrayingMantis";
	public string roomName = "Test Room";
	
	public string serverIP = "localhost";
	public int serverPort = 345678;
	public int maxConnections = 3;
	
	public int GameScene = 1;
	
	public GameObject servers;
	
	public UILabel errors;
	
	void Start() {
		Refresh();
	}
	
	public void Refresh() {
		MasterServer.ClearHostList();
		MasterServer.RequestHostList(gameName);
	}
	
	void Update() {
		HostData[] conns = MasterServer.PollHostList();
		
		/* for(int i=0; i<conns.Length-servers.transform.childCount; i++) {
			Transform bo = servers.transform.GetChild(0);
			Transform b = Instantiate(bo,bo.position,bo.rotation) as Transform;
			b.parent = servers.transform;
			b.position = Vector3.up*20f;
		} */
		
		for(int i = 0; i<conns.Length; i++) {
			setServer(conns[i],servers.transform.GetChild(i));
			

			//UIButton b = Instantiate(UIButton) as UIButton;
			//b.transform.parent = transform;
		}
	}
	
	private void setServer(HostData server, Transform t) {
		string name = server.gameName;
		string players = ""+server.connectedPlayers;
		
		ConnectToServerButton b = t.GetComponent<ConnectToServerButton>();
		b.setConnection(server);
		
		Transform animation = t.GetChild(0);

		UILabel l = animation.FindChild("ServerName").GetComponent<UILabel>();
		l.text = name+" "+players;
	}
}
