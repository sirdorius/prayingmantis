using UnityEngine;
using System.Collections;


public class MyMasterServer : MenuState {
	public Lobby lobby;
	
	public string gameName = "PrayingMantis";
	public string roomName = "Test Room";
	
	public string serverIP = "localhost";
	public int serverPort = 345678;
	public int maxConnections = 3;

	public GameObject ninjaPrefab;
	
	public Vector3 ninjaSpawn;
	
	private string choice;
	
	private NetworkConnectionError error_message;
	
	public int GameScene = 1;
	
	void Start() {
		Refresh();
		DontDestroyOnLoad(gameObject);
	}
	
	// Use this for initialization
	override public void OnGui () {
		menu.backgroundStyle.padding.top = Screen.height/4;
		
		menu.buttonStyle.margin.top = 0;
		menu.buttonStyle.margin.bottom = 0;
		
		if(!Network.isServer && !Network.isClient){
			
			roomName = GUILayout.TextField (roomName);
			if(GUILayout.Button("New Server", menu.buttonStyle)) {
				bool pa = Network.HavePublicAddress();
				Network.InitializeServer(maxConnections,serverPort, !pa);
				MasterServer.RegisterHost(gameName,roomName);
				
				choice = "Ninja";
				Application.LoadLevel(GameScene);
			}
			
			GUILayout.BeginHorizontal();
			GUILayout.Label("Game port:");
			serverPort = int.Parse(GUILayout.TextField(""+serverPort));
			GUILayout.EndHorizontal();
			
			GUILayout.BeginHorizontal();
			
			if(GUILayout.Button("Connect to:", menu.buttonStyle)) {
				Network.Connect(serverIP,serverPort);
				
				choice = "Soldier";
				Application.LoadLevel(GameScene);
			}
			serverIP = GUILayout.TextField(serverIP);
			
			GUILayout.EndHorizontal();

			if (GUILayout.Button("Refresh", menu.buttonStyle)) {
				Refresh();
			}
			GUILayout.BeginVertical();
	
			HostData[] conns = MasterServer.PollHostList();
			for(int i = 0; i<conns.Length; i++) {
				GUILayout.BeginHorizontal();
				GUILayout.Label("Name:"+conns[i].gameName);
				GUILayout.Label("Players:"+conns[i].connectedPlayers);
				if(GUILayout.Button("Join", menu.buttonStyle)) {
					NetworkConnectionError e = Network.Connect (conns[i]);
					
					choice = "Soldier";
				Application.LoadLevel(GameScene);
				}
				GUILayout.EndHorizontal();
			}
			GUILayout.EndVertical();
			if(error_message != NetworkConnectionError.NoError) {
				if(GUILayout.Button(error_message.ToString())) {
					error_message = NetworkConnectionError.NoError;
				}
			}
			if(GUILayout.Button("Exit")) {
				Application.Quit();
			}

		}
	}
	
	void Refresh() {
		MasterServer.ClearHostList();
		MasterServer.RequestHostList(gameName);
	}
	
	void Update () {

	}
	
	void OnLevelWasLoaded () {
		if(Network.isServer) {
			if(choice == "Ninja") {
						
				Network.Instantiate(ninjaPrefab, ninjaSpawn, Quaternion.identity, 0);
	
				Destroy(gameObject);
			}
		}
	}
	
	void OnConnectedToServer() {
		if(choice == "Soldier") {
			menu.setState(lobby);
		}
	}
	
	void OnFailedToConnect(NetworkConnectionError error) {
        error_message = error;
    }
	
	public void addServer(int serverPort, string name, int maxConnections) {
		
		MasterServer.RegisterHost("PrayingMantis",name);
	}
}
