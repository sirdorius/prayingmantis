using UnityEngine;
using System.Collections;

public class MainMenu : MenuState {
	public MyMasterServer online;
	
	override public void OnGui() {
		menu.buttonStyle.margin.top = getOffset(Screen.width, menu.buttonStyle.padding.top*2, 4)/2;
		menu.buttonStyle.margin.bottom = getOffset(Screen.width, menu.buttonStyle.padding.bottom*2, 4)/2;
		
		if(GUILayout.Button("Training", menu.buttonStyle)) {
		}
		else if(GUILayout.Button("Online", menu.buttonStyle)) {
			menu.setState(online);
		}
		else if(GUILayout.Button("Credits", menu.buttonStyle)) {
		}
		else if(GUILayout.Button("Exit", menu.buttonStyle)) {
			Application.Quit();
		}
		// Restituisce il bottone selezionato
		//GUI.tooltip;
	}
}
