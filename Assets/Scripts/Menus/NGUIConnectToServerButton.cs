using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class NGUIConnectToServerButton : MonoBehaviour {
	public NGUIOnlineMenu onlinemenu;
	private HostData connection;
	
	public UILabel serverIP;
	public UILabel serverPort;
	
	void Start() {
		if(serverIP!= null && serverPort!=null) {
			serverIP.text = onlinemenu.serverIP;
			serverPort.text = ""+onlinemenu.serverPort;
		}
	}
	
	// Use this for initialization
	void OnClick () {
		if(connection != null) {
			Network.Connect (connection);
			onlinemenu.errors.text = "connecting...";
		}
		else if(serverIP!= null && serverPort!=null) {
			Network.Connect(serverIP.text, int.Parse(serverPort.text));
			onlinemenu.errors.text = "connecting...";
		} else {
			print ("Lo script non sa a chi connettersi");
		}
	}
	
	void OnConnectedToServer() {
		Network.isMessageQueueRunning = false;
		HOTween.To(Camera.main.transform, 0.2f, new TweenParms().Prop("position", Vector3.right*-39.02726f));
	}
	
	public void setConnection(HostData con) {
		connection = con;
	}
}
