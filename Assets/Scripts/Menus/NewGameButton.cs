using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NewGameButton : MonoBehaviour {
	public OnlineMenu onlinemenu;
	public MatchInitialization matchInitialization;
	public Text playerName;
	public Text serverPort;
	
	public void OnClick() {
		bool pa = Network.HavePublicAddress();
		
		NetworkConnectionError error;

		if(serverPort==null) {
			error = Network.InitializeServer(onlinemenu.maxConnections,onlinemenu.serverPort, !pa);
		} else {
			error = Network.InitializeServer(onlinemenu.maxConnections,int.Parse(serverPort.text), !pa);
		}
		
		if(error == NetworkConnectionError.NoError) {
			MasterServer.RegisterHost(onlinemenu.gameName,playerName.text + "'s game");
			matchInitialization.setChoice("Ninja", playerName.text);
			Application.LoadLevel(onlinemenu.GameScene);
		}
		else {
			onlinemenu.errors.text = ""+error;
		}
	}
	
	void OnFailedToConnect(NetworkConnectionError error) {
        onlinemenu.errors.text = ""+error;
    }
}
