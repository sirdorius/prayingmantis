using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OnlineMenu : MonoBehaviour {
	public string gameName = "PrayingMantis";
	public string roomName = "Test Room";
	
	public string serverIP = "localhost";
	public int serverPort = 345678;
	public int maxConnections = 3;
	
	public int GameScene = 1;
	
	public GameObject servers;
	
	public Text errors;

    public Transform server_prefab;
	
	void Start() {
		Refresh();
        // server_prefab = servers.transform.GetChild(0);
	}
	
	public void Refresh() {
		MasterServer.ClearHostList();
		MasterServer.RequestHostList(gameName);
	}
	
	void Update() {
		HostData[] conns = MasterServer.PollHostList();

        int child = servers.transform.childCount;

        for (int i = 0; i < conns.Length - child; i++) {
            RectTransform b = Instantiate(server_prefab) as RectTransform;
            b.parent = servers.transform;
            b.localPosition = server_prefab.localPosition - Vector3.up * 20f * (child + i);
            b.localRotation = Quaternion.identity;
            b.localScale = Vector3.one;
        }
		
		for(int i = 0; i<child && i<conns.Length; i++) {
			setServer(conns[i],servers.transform.GetChild(i));
			

			//UIButton b = Instantiate(UIButton) as UIButton;
			//b.transform.parent = transform;
		}
	}
	
	private void setServer(HostData server, Transform tr_server) {
		string name = server.gameName;
		string players = ""+server.connectedPlayers;

        Transform connect = tr_server.FindChild("Connect");

		ConnectToServerButton b = connect.GetComponent<ConnectToServerButton>();
		b.setConnection(server);

        Transform servername = tr_server.FindChild("ServerName");

		Text l = servername.GetComponentInChildren<Text>();
        l.text = name;

        Transform numPlayers = tr_server.FindChild("NumPlayers");

        l = numPlayers.GetComponentInChildren<Text>();
		l.text = players;
	}
}
