using UnityEngine;
using System.Collections;
using Holoville.HOTween;

[RequireComponent(typeof (Animator))]
[RequireComponent(typeof (Collider))]
[RequireComponent(typeof (Rigidbody))]
[RequireComponent(typeof (AudioSource))]

public class Character : MonoBehaviour {
	public float acceleration;
	public float timeToLookAtCursor;
	
	protected float defaultAccel;
	protected Vector3 velocity;
	
	public float timeToRespawn = 2f;

	protected MyGUI HUD;

	public int health = 100;
	private int healthLeft;

	public Vector3 movementAxeses;
	
	private int frameVisible;
	public int armor = 0;
	
	public float audioDelay = 0.5f;
	
	public string username;

	public Bullet bullet;

    protected Vector2 direction;
    protected Vector2 deltaPos;
    protected Vector2 lastRemotePos;
    protected float zoom;
    protected Animator anim;
    protected bool isKilled;

    private float CamDestinationSize;
	
	void Awake() {
		HOTween.Init(false, false, true);
		HOTween.EnableOverwriteManager(false);
		anim = GetComponent<Animator>();
		CamDestinationSize = Camera.main.orthographicSize;
	}
	
	// Use this for initialization
	virtual protected void Start () {
		if(networkView.isMine)
			OnConnect();

		defaultAccel = acceleration;
		if (!networkView.isMine) {
			transform.FindChild("light").light.enabled = false;
			Destroy(GetComponent<AudioListener>());
		}
		
	}
	
	public virtual void OnConnect() {
		Camera.main.GetComponent<FollowPlayer>().player = transform;
		
		gameObject.AddComponent<AudioListener>();
		GameObject HUD_GO = GameObject.Find ("HUD");

		if(HUD_GO) {
			HUD = HUD_GO.GetComponent<MyGUI>();
			HUD.setVisible(true);
			healthLeft = health;
			HUD.setHealth(healthLeft);
		}
		else {
			Debug.LogWarning(name+" HUD has a null reference");
		}
		
		/*textName.GetComponent<PlayerName>().target = gameObject.transform;
		textName.GetComponent<PlayerName>().setName(nameCharacter);
		textName.GetComponent*/
	}
	
	// Update is called once per frame
	virtual protected void Update () {
        AudioHandler();

        if (isKilled) return;

		if (networkView.isMine) {
			ControlPolicy();
			anim.SetFloat("Speed", rigidbody.velocity.magnitude);
			
			float rot = Mathf.Rad2Deg * Mathf.Atan2 (-direction.y, direction.x);
			HOTween.To(transform, timeToLookAtCursor, new TweenParms().Prop("rotation", new Vector3(0,rot+90,180)));
			// zoom camera if scrolling
		}
		else {
			goTo (transform.position);
		}
	}

	public void goTo(Vector3 position) {
		deltaPos = Vector2.right*(position.x-lastRemotePos.x) +
			Vector2.up*(position.z-lastRemotePos.y);

		deltaPos = deltaPos.normalized;

		lastRemotePos = Vector2.right*transform.position.x + Vector2.up*transform.position.z;
	}

	public void Stop() {
		deltaPos = Vector2.zero;
		lastRemotePos = Vector2.right*transform.position.x + Vector2.up*transform.position.z;
	}
	
	void FixedUpdate() {
		if ((Network.isServer || Network.isClient) && !networkView.isMine) return;
		rigidbody.AddForce(deltaPos.x*acceleration
		                   ,0,
		                   deltaPos.y*acceleration,
		                   ForceMode.VelocityChange);
	}
	
	void AudioHandler() {
		if(deltaPos.magnitude>0.01f) {
			if(!audio.isPlaying) {
				audio.PlayDelayed(audioDelay);
			}
		}
	}
	
	virtual protected void ControlPolicy () {
		// move with keyboard
		float deltaX = Input.GetAxis ("Horizontal");
		float deltaY = Input.GetAxis ("Vertical");
		deltaPos = Vector2.right*deltaX + Vector2.up*deltaY;
		
		// rotate to cursor
		var ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;
		Physics.Raycast(ray, out hit, 1000, 1<<8);
		Vector3 d = transform.position - hit.point;
		direction = d.x*Vector2.right+d.z*Vector2.up;
		
		// shift makes you walk
		if (Input.GetKeyDown(KeyCode.LeftShift)) {
			acceleration = defaultAccel/3;
			audio.volume /= 2;
			audioDelay *= 2;
		}
		else if (Input.GetKeyUp(KeyCode.LeftShift)) {
			acceleration = defaultAccel;
			audio.volume *= 2;
			audioDelay /= 2;
		}
		
		zoom = Input.GetAxis("Mouse ScrollWheel");
		CamDestinationSize *= 1 - zoom * 1f;
		if (CamDestinationSize > 50)
			CamDestinationSize = 50;
		else if (CamDestinationSize < 4)
			CamDestinationSize = 4;
		HOTween.To(Camera.main, 0.2f, new TweenParms().Prop("orthographicSize", CamDestinationSize));
	}
	
	public void setInput(Vector2 deltaPos, Vector2 direction, float zoom) {
		this.direction = direction;
		this.deltaPos = deltaPos;
		this.zoom = zoom;
	}

	public void setDirection(Vector3 d) {
		setDirection(Vector2.right * d.x + Vector2.up * d.z);
	}

	public void setDirection(Vector2 d) {
		direction = d;
	}

	public Vector2 getDirection() {
		return direction;
	}
	
	public void setVisible(bool v) {
		if(!networkView.isMine) {
			// Se qualcuno mi ha scovato non posso nascondermi
			if(frameVisible == Time.frameCount) return;
			
			if(v==true) frameVisible = Time.frameCount;
			Renderer[] parts = GetComponentsInChildren<Renderer>();
			foreach(Renderer part in parts)
				part.renderer.enabled = v;
		}
	}

	virtual public void killed () {
		healthLeft = health;
        startAnimation("Death");
        transform.FindChild("DeathBlood").particleSystem.Emit(50);
        transform.FindChild("VisionCone").GetComponent<VisionCone>().enabled = false;
        transform.FindChild("VisionCone").renderer.enabled = false;
        collider.enabled = false;
        enabled = false;

        Invoke("findRespawn", timeToRespawn);
	}

    private void findRespawn() {
        Respawner.respawn(this);
    }

    virtual public void AfterRespawn() {
        isKilled = false;

        startAnimation("Respawn");

        transform.FindChild("VisionCone").GetComponent<VisionCone>().enabled = true;
        transform.FindChild("VisionCone").renderer.enabled = true;
        collider.enabled = true;
        enabled = true;
    }
	
	virtual protected void OnCollisionEnter(Collision coll) {
	}
	
	[RPC]
	public void getDamage(int damage) {
		transform.FindChild("HitBlood").particleSystem.Emit(10);
		//Debug.LogError("ciccia :"+name+" "+networkView.owner);
		if(networkView.isMine) {
			if(true) {
				int damageTaken = damage - armor;
				if (healthLeft - damageTaken > 0) {
					healthLeft -= damageTaken;
				} else {
					healthLeft = 0;
					killed();
				}
				HUD.setHealth(healthLeft);
			}
		}
	}
	
	public void stunChar(float time) {
		StartCoroutine(StunTime(time));
	}
	
	IEnumerator StunTime(float time) {
		print("Coroutine time:"+time);
		
		enabled = false;
		
		yield return new WaitForSeconds(time);
		
		enabled = true;
	}
	
	void OnPlayerDisconnected(NetworkPlayer player) {
		if(networkView.owner == player)
			Network.Destroy(gameObject);
	}
	
	void OnDisconnectedFromServer(NetworkDisconnection mode) {
		Application.LoadLevel("Menu");
	}

	public void startAnimation(string name) {
		anim.SetBool(name, true);
		StartCoroutine(stopAnim(name));
	}
	
	public IEnumerator stopAnim(string name){
		yield return new WaitForSeconds(anim.GetNextAnimatorStateInfo(0).length);
		anim.SetBool(name, false);
	}

	public Animator getAnimator() {
		return anim;
	}

	[RPC]
	public void setName(string name) {
		username = name;
		var a = transform.FindChild("username");
		a.GetComponent<TextMesh>().text = username;
		a.transform.FindChild("usernameshadow").GetComponent<TextMesh>().text = username;
	}
}
