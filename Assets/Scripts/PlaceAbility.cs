using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class PlaceAbility : Skills {
	public GameObject prefab;
	public int max;
	private int num;

	private GameObject[] instances;

	void Start() {
		instances = new GameObject[max];
	}

    override public void activate(Character c) {
	    Vector3 d = Vector3.right * c.getDirection().x +
	        Vector3.forward * c.getDirection().y;
	    d.Normalize();
	    Vector3 deltaPos = -d / d.magnitude;


	    Vector3 pos = c.transform.position + deltaPos * 2f;
		Quaternion rotation = Quaternion.Euler (transform.rotation.eulerAngles.x,
		                                        transform.rotation.eulerAngles.y + 180f, 0);
	    GameObject instance = Network.Instantiate(prefab, pos, rotation, 0) as GameObject;

		addInstance(instance);
    }

	void addInstance(GameObject instance) {
		if(num<max) {
			instances[num] = instance;
			num++;
		}
		else {
			Network.Destroy(instances[0]);

			for(int i=0;i<max-1;i++) {
				instances[i] = instances[i+1];
			}

			instances[max-1] = instance;
		}

		PlaceObject placeO = instance.GetComponent<PlaceObject> ();
		if(placeO!=null) placeO.setPlacer (this);
	}

	public void removeInstance(GameObject g) {
		int i;
		for (i=0; i<max; i++) {
			if(instances[i]==g) {
				num--;
				break;
			}
		}
		for (; i<max-1; i++) {
			instances[i] = instances[i+1];
		}
	}
}
