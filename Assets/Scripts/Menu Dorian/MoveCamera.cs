using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class MoveCamera : MonoBehaviour {
	public Vector3 newPosition;
	
	public void OnClick() {
		HOTween.To(Camera.main.transform, 0.2f, new TweenParms().Prop("position", newPosition));
	}
}
