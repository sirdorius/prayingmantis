using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class ExitButton : MonoBehaviour {
	
	public void OnClick() {
		Application.Quit();
	}
}
