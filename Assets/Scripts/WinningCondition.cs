using UnityEngine;
using System.Collections;

public class WinningCondition : MonoBehaviour {
	public  int totalObjectives = 1;
	private int objectivesCount = 0;
	
	private string winningtext = "";
	public GUIStyle background;
	public GUIStyle label;
	
	private int width = 300;
	private int height = 300;
	
	void Start() {
		//totalObjectives = GameObject.FindGameObjectsWithTag("Objective").Length;
		
	}
	
	public void ObjectCaught() {
		objectivesCount ++;
		
		if( objectivesCount == totalObjectives ) {
			Time.timeScale = 0;
			winningtext = "Ninja win";
		}
	}
	
	public int getObjectiveCount() {
		return objectivesCount;
	}
	
	public void soldiersWin() {
		Time.timeScale = 0;
		winningtext = "Soldiers wins";
	}
	
	void OnGUI () {
		if(winningtext != "" ) {
			int startx = (Screen.width-width)/2;
			int starty = (Screen.height-height)/2;
			
			GUILayout.BeginArea(new Rect(startx,starty,width,height), background);
			GUILayout.Label(winningtext);
			if(GUILayout.Button("Continue", label)) {
				Network.Disconnect();
				Time.timeScale = 1;
				Application.LoadLevel("Menu");
			}
			GUILayout.EndArea();
		}
	}
}
