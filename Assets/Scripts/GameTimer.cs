using UnityEngine;
using System.Collections;

[RequireComponent (typeof(GUIText))]
[RequireComponent (typeof(NetworkView))]

public class GameTimer : MonoBehaviour {
	public float timeNeededInMinute;
	
	public int width;
	public float height;

	public float topoffset;
	
	public WinningCondition wincond;
	
	private GUIText timerGUIText;
	
	void Awake() {
		timeNeededInMinute*=60;
		timerGUIText = GetComponent<GUIText>();
		timerGUIText.pixelOffset = new Vector2((Screen.width-width)/2, Screen.height - topoffset);
	}
	
	void OnGUI() {
		if(Network.isServer) timeNeededInMinute -= Time.deltaTime;
		if(timeNeededInMinute<=0){
			wincond.soldiersWin();
		}
		
		timerGUIText.text = toString(timeNeededInMinute);
		//GUI.Label(new Rect( (Screen.width-width)/2, topoffset, width, height), toString(timeNeededInMinute));
	}
	
	 void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) {
        if (stream.isWriting) {
			int temp = (int)timeNeededInMinute;
            stream.Serialize(ref temp);
        } else {
			int temp = 0;
            stream.Serialize(ref temp);
			timeNeededInMinute = temp;
        }
    }
	
	private string toString(float time) {
		int min = (int) time/60;
		int sec = (int) time%60;
		return string.Format("{0:00}:{1:00}", min, sec);
	}
}