using UnityEngine;
using System.Collections.Generic;


public class Graph {
	
	private Dictionary<string, Node> nodes =  new Dictionary<string, Node>();
    private List<Node> list = new List<Node>();
	private MyKDTree tree = null;
	private float NODE_DISTANCE;
	private float RADIUS;
    private LayerMask mask;

	private List<Node> closeSet = new List<Node>();
	private List<Node> openSet = new List<Node>();

	public Graph(float distance, float radius, LayerMask mask) {
		NODE_DISTANCE = distance;
		RADIUS = radius;
        this.mask = mask;
	}
	
	public void addNode(Node n) {
		nodes[n.getPosition().ToString()] = n;
        list.Add(n);
	}
	
	public Node getNode(string pos) {
		if (nodes.ContainsKey(pos)) {
			return nodes[pos];
		} else {
			return null;
		}
	}

	public void setTree(MyKDTree tree) {
		this.tree = tree;
	}

	public List<Node> getNodes() {
		return list;
	}
	
	public void setNeighboors() {

		Vector3[] directions = new Vector3[4];

		Node value;
		foreach(Node n in list) {
            directions[0] = directions[1] = directions[2] = directions[3] = n.getPosition();

			directions[0].z += NODE_DISTANCE;
			directions[1].z -= NODE_DISTANCE;
			directions[2].x -= NODE_DISTANCE;
			directions[3].x += NODE_DISTANCE;


			foreach(Vector3 v in directions) {
				if (this.nodes.TryGetValue(v.ToString(), out value)) {
					RaycastHit hit;
					if (Physics.Linecast(n.getPosition(), v, out hit)) {
						Debug.Log("Nodo adiacente non raggiungibile (non lo considero un vicino) " + hit.collider.name + " " +
                            hit.collider.transform.position);
					} else {
						n.addNeighboor(value);
					}

				}
			}
		}

		List<IComparer<Node>> comparers = new List<IComparer<Node>>();
		comparers.Add(new XComparer()); 
        comparers.Add(new ZComparer());

		tree = new MyKDTree(2, list, comparers);

	}

	public List<Vector3> Astar(Vector3 start, Vector3 target) {

		Node begin = tree.KNNSearch(new Node(start), tree.root);
		Node end = tree.KNNSearch(new Node(target), tree.root);

		if (openSet.Count > 0) {
			foreach (Node n in openSet) {
				n.ParentNode = null;
				n.Value = float.PositiveInfinity;
			}
			openSet.Clear();
		}
		if (closeSet.Count > 0) {
			foreach(Node n in closeSet) {
				n.ParentNode = null;
				n.Value = float.PositiveInfinity;
			}
			closeSet.Clear();
		}

		ListValueComparer openSetComparer = new ListValueComparer();

		begin.Value = begin.getDistance(end);
		openSet.Add(begin);

		while(openSet.Count > 0) {
			openSet.Sort(openSetComparer);
			Node current = openSet[0];

			if(current.getPosition().Equals(end.getPosition())) {
				return smooth(getShortestPath(current), RADIUS);
			}

			openSet.RemoveAt(0);
			List<Node> neighboors = current.getNeighboors();

			foreach(Node neighboor in neighboors) {
				if (!closeSet.Contains(neighboor)) {
					float heuristicValue = current.getDistance(neighboor.getPosition()) + neighboor.getDistance(end.getPosition());
					float value;

					int index = openSet.IndexOf(neighboor);
					if (index >= 0) {
						if (openSet[index].Value > heuristicValue) {
							openSet[index].Value = heuristicValue;
							neighboor.ParentNode = current;
						}
					} else {
						neighboor.Value = heuristicValue;
						neighboor.ParentNode = current;
						openSet.Add(neighboor);
					}
				}
			}
			closeSet.Add(current);
		}

		return null;
	}

	private List<Node> getShortestPath(Node n) {
		List<Node> shortestPath = new List<Node>();

		shortestPath.Add(n);
		Node current = n;
		while (current.ParentNode != null) {
			shortestPath.Insert(0, current.ParentNode);
			current = current.ParentNode;
		}

		return shortestPath;
	}

	private List<Vector3> smooth(List<Node> path, float RADIUS) {


		Vector3 prevPos, targetPos, sideLinecast, linecastPos;
		Node previous, target, previousForLinecast;
		List<Vector3> smoothedPath = new List<Vector3>();
		previousForLinecast = previous = path[0];
		IEnumerator<Node> it = path.GetEnumerator();
		prevPos = previous.getPosition();
		it.MoveNext();

		while (it.MoveNext()) {
			target = it.Current;
			RaycastHit hit;
			targetPos = target.getPosition();
			linecastPos = previousForLinecast.getPosition();
			//float distance = Mathf.Sqrt(target.getDistance(pos));
			/*if(Physics.Linecast(prevPos, targetPos, out hit)) {
				smoothedPath.Add(previous.getPosition());
				prevPos = previous.getPosition();
			} else {

				if (ANGLE > 0) {
					sideLinecast = Quaternion.AngleAxis(ANGLE, Vector3.up) * targetPos;
					if(Physics.Linecast(linecastPos, sideLinecast, out hit, mask)) {
						smoothedPath.Add(previousForLinecast.getPosition());
						prevPos = previous.getPosition();
					} else  {
						sideLinecast = Quaternion.AngleAxis(-ANGLE, Vector3.up) * targetPos;
						if (Physics.Linecast(linecastPos, sideLinecast, out hit, mask)) {
							smoothedPath.Add(previousForLinecast.getPosition());
							prevPos = previous.getPosition();
						}
					}
				}
			} */
            if (Physics.CheckCapsule(prevPos, targetPos, RADIUS)) {
                smoothedPath.Add(previous.getPosition());
                prevPos = previous.getPosition();
            }
			previousForLinecast = previous = target;

		}

	
		smoothedPath.Add(path[path.Count - 1].getPosition());

		return smoothedPath;

	}

    public void drawNodes() {
        foreach(Node n in list) {
            Debug.DrawLine(n.getPosition(), n.getPosition() + Vector3.up * 6f, Color.blue);
        }
    } 
}