﻿using UnityEngine;
using System.Collections.Generic;

public class ValueComparer : IComparer<Node> {
	
	public int Compare (Node x, Node y)
	{
		if (x.getPosition().x == y.getPosition().x && x.getPosition().z == y.getPosition().z) {
			return 0;
		} else {
			int result = x.Value.CompareTo(y.Value);
			if(result == 0) {
				return 1;
			} else return result;
		
		}
	}
}
