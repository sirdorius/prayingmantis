﻿using UnityEngine;
using System.Collections.Generic;

public class MyKDTree {

	public int dimension {
		get;
		set;
	}

	public KdTreeNode<Node> root;

	private IList<IComparer<Node>> comparers = default(IList<IComparer<Node>>);

	public MyKDTree(int dimension, List<Node> list, IList<IComparer<Node>> comparers) {
		this.dimension = dimension;
		this.comparers = comparers;
		root = MyKDTreeConstruct(list, 0, list.Count - 1);
	}

	public KdTreeNode<Node> MyKDTreeConstruct(List<Node> list,
	                                             int startIndex, int endIndex, int depth = 0) {
		int length = endIndex - startIndex + 1;

		if (length <= 0) return null;

		//root = new KdTreeNode<Node>(v);
		int axis = depth % dimension;

		if (list.Count != 1) list.Sort(startIndex, length, comparers[axis]);
		int median = startIndex + length / 2;

		KdTreeNode<Node> node = new KdTreeNode<Node>(list[median]);

		node.LeftChild = MyKDTreeConstruct(list, startIndex, median - 1, depth + 1);
		node.RightChild = MyKDTreeConstruct(list, median +1, endIndex, depth + 1);

		return node;
	}

	public Node KNNSearch(Node v, KdTreeNode<Node> check, Node currentBest = default(Node), int depth = 0) {

        //sono arrivato alla fine dell'albero
		if (check == null) return currentBest;

		int dim = depth % this.dimension;

		Node retValue = default(Node);

		if (check.LeftChild == null && check.RightChild == null) {
			return check.Value;
		} else {
			float distance = v.getDistance(check.Value);
			float bestDistance = v.getDistance(currentBest);
			float otherPlaneDistance = float.PositiveInfinity;

			if (comparers[dim].Compare(v, check.Value) <= 0) {
				currentBest = KNNSearch(v, check.LeftChild, currentBest, depth + 1);

				bestDistance = v.getDistance(currentBest);

				if (distance < bestDistance) {
					bestDistance = distance; 
					currentBest = check.Value;
				}

				if (bestDistance >= check.Value.getCoordinateDifference(v, dim)) {
					Node otherPlane = KNNSearch(v, check.RightChild, currentBest, depth + 1);

					otherPlaneDistance = v.getDistance(otherPlane);

					if (otherPlaneDistance < bestDistance) {
						bestDistance = otherPlaneDistance; 
						currentBest = otherPlane;
					}
				}



			} else {
				currentBest = KNNSearch(v, check.RightChild, currentBest, depth + 1);

				bestDistance = v.getDistance(currentBest);

				if (distance < bestDistance) {
					bestDistance = distance; 
					currentBest = check.Value;
				}
				
				if (bestDistance >= check.Value.getCoordinateDifference(v, dim)) {
					Node otherPlane = KNNSearch(v, check.LeftChild, currentBest, depth + 1);
					
					otherPlaneDistance = v.getDistance(otherPlane);

					if (otherPlaneDistance < bestDistance) {
						bestDistance = otherPlaneDistance; 
						currentBest = otherPlane;
					}
				}
				

			}

			return currentBest;
				
		}

	}

	/*private KdTreeNode<Node> findNearestChild(KdTreeNode<Node> check, Node v) {
		KdTreeNode<Node> retValue = default(KdTreeNode<Node>);
		double distLeft = 0f;
		double distRight = 0f;
		if (check.LeftChild != null) {
			distLeft = v.getDistance(check.LeftChild.Value);
		}
		if (check.RightChild != null) {
			distRight = v.getDistance(check.RightChild.Value);
		}

		return (distLeft > distRight) ? check.LeftChild : check.RightChild;

	}*/

	public override string ToString ()
	{
		return string.Format (root.ToString());
	}

}
