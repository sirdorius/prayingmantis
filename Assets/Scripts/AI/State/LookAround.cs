﻿using UnityEngine;
using System.Collections;

public class LookAround : MonoBehaviour {
    public Character character;
    public float minDistance = 5f;
    public float time =3f;
    private float timeCount = 0;

    void Update() {
        timeCount += Time.deltaTime * Time.timeScale;
        if (timeCount >= time) {
            UpdateDirection();

            Vector3 direction = Vector3.right * character.getDirection().x + Vector3.forward * character.getDirection().y;
            if(minDistance==0 || Physics.Raycast(transform.position, direction, minDistance)) {
                timeCount = 0;
            }
        }
    }

    void UpdateDirection() {
        float radiant = Random.value * Mathf.PI * 2;

        Vector2 direction;
        direction = Vector2.right * Mathf.Cos(radiant) + Vector2.up * Mathf.Sin(radiant);

        character.setDirection(direction);
    }
}
