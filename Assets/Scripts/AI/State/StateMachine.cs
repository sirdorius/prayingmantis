﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StateMachine : MonoBehaviour {
	private Dictionary<string,State> states;
	private State currentState;

	protected virtual void Awake() {
		states = new Dictionary<string,State> ();
	}

	public void setState(string name) {
		setState(states[name]);
	}

	public void setState(State state) {
		if(currentState!=null) currentState.OnExit ();
		currentState = state;
		currentState.OnEnter ();
	}

	public State getState() {
		return currentState;
	}

	public void AddState(string name, State state) {
		states.Add (name, state);
		state.setFSM (this);
	}

	protected virtual void Update() {
        if (currentState != null) currentState.Action();
        else Debug.LogWarning("No state found");
	}
}
