﻿using UnityEngine;
using System.Collections;

public class Path : MonoBehaviour, ObjectiveObserver {
    public Objective[] objectives;
    public SoldierAIDirector director;

    void Start() {
        foreach(Objective objective in objectives) {
            objective.AddObserver(this);
        }
    }

    public void ObjectiveCaught(Objective objective) {
        int count = 0;
        for (int i = 0; i < objectives.Length; i++ ) {
            if (objectives[i] == null) count++;
            else {
                if (objective == objectives[i]) {
                    objectives[i] = null;
                    count++;
                }
            }
        }
        if (count == objectives.Length) {
            director.removePath(gameObject);
            Destroy(gameObject);
        }
    }
}
