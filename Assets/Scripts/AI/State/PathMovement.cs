﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathMovement : MonoBehaviour {
	private int pathIndex = 0;
	public float threshold = 50;
	public List<GameObject> path;
	public bool loop = true;
	
	private Vector3 startPosition;
	
	private bool _loopedOnce = false;
	public bool loopedOnce {
		get{
			return _loopedOnce;
		}
	}
	
	private Character c;
	
	// Use this for initialization
	void Start () {
		c = GetComponent<Character>();
		startPosition = transform.position;
	}
	
	public void restart(){
		pathIndex = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if(path.Count>0 && pathIndex<path.Count){
			Vector3 pos = getNextObjective();
			Vector3 objectPosition = Vector3.right * transform.position.x +
								Vector3.forward * transform.position.z;

			if( Vector3.Distance(objectPosition,pos)<threshold ){
				pathIndex ++;
				if( pathIndex==path.Count && loop) {
					pathIndex = 0;
					_loopedOnce = true;
				}
			}
			c.goTo(pos);
		}
	}

	public void setPath(GameObject g) {
		path.Clear ();

		for (int i=0; i<g.transform.childCount; i++) {
			path.Add(g.transform.GetChild(i).gameObject);
		}
	}
	
	public Vector3 getNextObjective() {
		Vector3 pos;
		pos.x = path[pathIndex].transform.position.x;
		pos.y = 0;
		pos.z = path[pathIndex].transform.position.z;
		return pos;
	}
}
