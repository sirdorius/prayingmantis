﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FollowPathState : State {
    public float maxDistancePath;

    private string nextState;
    private LookAround lookAround;
    private List<Vector3> path;
    private Vector3 destination;
    private float stopDistance = 1f;
    private int index;
    private Character character;

	// Use this for initialization
	void Awake () {
        lookAround = GetComponent<LookAround>();
        character = GetComponent<Character>();
	}
	
	// Update is called once per frame
	void Update () {
        // Action();
	}

    public override void Action() {
        float distance = (transform.position - destination).magnitude;

        for (int i = index; i < path.Count; i++) {
            Debug.DrawLine(path[i],path[i]+Vector3.up*10f, Color.yellow);
        }

        if (distance < stopDistance) {
            setFSMState(nextState);
        }
        else {
            Vector3 path_position = path[index];
            distance = (transform.position - path_position).magnitude;
            
            character.goTo(path_position);

                if (distance < stopDistance) {
                    index++;
                    if(index < path.Count)
                        path_position = path[index];
                    else setFSMState(nextState);
                }
            }
    }

    public bool setDestination(Vector3 point, string nextState, bool tooLongerCheck = false) {
        this.nextState = nextState;
        index = 0;
        destination = point;
        path = AStar.setDestination(transform.position, point);
        if (path == null) {
            setFSMState(nextState);
            return false;
        }

        if (tooLongerCheck) {
            if(getPathLenght() < maxDistancePath) {
                setFSMState("path_finding");
                return true;
            }
        }
        else {
            setFSMState("path_finding");
            return true;
        }

        return false;
    }

    public float getPathLenght() {
        float lenght = (path[0] - transform.position).magnitude;

        for (int i = 0; i < path.Count-1; i++) {
            lenght += (path[i + 1] - path[i]).magnitude;
        }

        return lenght;
    }

    public override void OnEnter() {
        lookAround.enabled = true;
    }

    public override void OnExit() {
        lookAround.enabled = false;
    }
}
