﻿using UnityEngine;
using System.Collections;

public class Pursue : State, NinjaDiscoverObserver {
	public NPC player;
	public Character ninja;
	public NinjaDiscover ninjaDiscover;
	public float minDistance = 5f;
    // Max distance to pursue the ninja
    // used by SoldierAI to decide whether pursue the ninja
    // when another soldier has found it
    public float maxPursueDistance = 50f;
	public Weapon weapon;

    public float anglePrecision;

    private SoldierAIDirector director;

	void Start() {
        SoldierAI stateMachine = (SoldierAI)getFSM();
        director = stateMachine.getDirector();
		ninjaDiscover.AddObserver (this);
	}

	public override void Action() {
        if (ninja == null) {
            SoldierAI stateMachine = (SoldierAI)getFSM();

            SoldierAIDirector director = stateMachine.getDirector();

            GameObject path = director.newPath();
            stateMachine.setPath(path);

            return;
        }

		Vector3 distanceVector = player.transform.position - ninja.transform.position;
        Vector3 playerDirection = player.getDirection().x * Vector3.right + player.getDirection().y * Vector3.forward;
        Vector2 orientationVector = distanceVector.x*Vector2.right + distanceVector.z*Vector2.up - player.getDirection();
        float angle = Mathf.Atan2(orientationVector.y,orientationVector.x);

        Quaternion error;

        if (angle < 0) {
            error = Quaternion.AngleAxis(anglePrecision, Vector3.up);
        }
        else {
            error = Quaternion.AngleAxis(-anglePrecision, Vector3.up);
        }
        distanceVector = error * distanceVector;

		player.setDirection (distanceVector);

		float distance = distanceVector.magnitude;

		if (distance > minDistance)
			player.goTo (ninja.transform.position);
		else
			player.Stop ();

		if(weapon.bulletsRemaning()==0) {
			weapon.reload();
		}
		weapon.shoot ();
	}
	
	public override void OnEnter() {
		enabled = true;
	}

	public override void OnExit() {
		enabled = false;
	}

	public void OnDiscoverEnter(Character c) {
        setFSMState("pursue");
        director.ninjaDiscovered((SoldierAI)getFSM(), c);

        ninja = c;
	}

	public void OnDiscoverStay(Character c) {
	}
	public void OnDiscoverExit(Character c) {
        SoldierAI ai = (SoldierAI) getFSM();

        bool isTooFarOrDestroyed = false;
        if (ninja != null) {
            isTooFarOrDestroyed = ai.setDestination(ninja.transform.position, "wandering", true);
        }

        if (!isTooFarOrDestroyed) {
            ai.newPath();
        }

        ninja = null;
	}
}
