﻿using UnityEngine;
using System.Collections;

public abstract class State : MonoBehaviour {
	private StateMachine stateMachine;

	public void setFSM(StateMachine s) {
		stateMachine = s;
	}

    public StateMachine getFSM() {
        return stateMachine;
    }

	protected void setFSMState(string s) {
		stateMachine.setState (s);
	}

	public abstract void Action();
	public abstract void OnEnter();
	public abstract void OnExit();
}
