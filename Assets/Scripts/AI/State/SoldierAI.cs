using UnityEngine;
using System.Collections;

public class SoldierAI : StateMachine {
	private Pursue pursue;
	private PatrolState patrol_state;
    private FollowPathState pathfinding;
    private Wandering wandering;
    private PlaceObjectState placeobject;

    private SoldierAIDirector director;

	protected override void Awake() {
        base.Awake();

        pursue = GetComponent<Pursue>();
		patrol_state = GetComponent<PatrolState>();
        pathfinding = GetComponent<FollowPathState>();
        wandering = GetComponent<Wandering>();
        placeobject = GetComponent<PlaceObjectState>();

		AddState ("patrol", patrol_state);
		AddState ("pursue", pursue);
        AddState("path_finding", pathfinding);
        AddState("wandering", wandering);
        AddState("place_object", placeobject);

		//setState (patrol_state);
	}

    public void setPath(GameObject path) {
        Transform point_transform = path.transform.GetChild(Random.Range(0, path.transform.childCount));
        patrol_state.setPath(path);

        setDestination(point_transform.position, "patrol");
    }

    public bool setDestination(Vector3 position, string nextState, bool tooLongerCheck=false) {
        return pathfinding.setDestination(position, nextState, tooLongerCheck);
    }

    public void newPath() {
        GameObject path;

        path = getPath();
        if (path != null) director.setPathOccipied(path, false);

        path = director.newPath();
        setPath(path);
    }

    public GameObject getPath() {
        return patrol_state.getPath();
    }

    public void ninjaDiscovered(Character ninja) {
        // If is not too far and do not pursue
        if(getState()!=pursue) {
            pathfinding.setDestination(ninja.transform.position, "pursue", true);
        }
    }

    public SoldierAIDirector getDirector() {
        return director;
    }

    public void setDirector(SoldierAIDirector director) {
        this.director = director;
    }

    public void placeObject(Vector3 point) {
        placeobject.setPlacePoint(point);
    }
}