﻿using UnityEngine;
using System.Collections;

public class PatrolGraphState : State, NinjaDiscoverObserver {
	public NinjaDiscover ninjaDiscover;
	public LookAround lookAround;

    private PatrolNode patrolNode;

    private GameObject path;

    private SoldierAIDirector director;

	void Awake() {
		OnExit ();
	}

	void Start() {
        SoldierAI stateMachine = (SoldierAI)getFSM();
        director = stateMachine.getDirector();
		ninjaDiscover.AddObserver (this);
	}

	public override void Action() {

	}

	public override void OnEnter() {
		lookAround.enabled = true;
		// pathMovement.setPath (path);
	}
	public override void OnExit() {
		lookAround.enabled = false;
	}

    public void setPath(GameObject path) {
        this.path = path;
    }

	public void OnDiscoverEnter(Character c) {
		setFSMState ("pursue");
        director.ninjaDiscovered((SoldierAI) getFSM(), c);
	}
	
	public void OnDiscoverStay(Character c) {

	}
	public void OnDiscoverExit(Character c) {
        director.setPathOccipied(path, false);
		// statemachine.setState(
	}
}
