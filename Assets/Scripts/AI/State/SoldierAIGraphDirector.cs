﻿using UnityEngine;
using System.Collections;

// Posizione path dettato dalla vicinanza al path piu' vicino?
// Posizione path dettato dalla ultima posizione del ninja?


public class SoldierAIGraphDirector : MonoBehaviour {
	public GameObject[] paths;
    public int NPCNumber;
    public GameObject NPCPrefab;

    private SoldierAI[] soldiers;

	public void setPathOccipied(GameObject path, bool value) {
		for (int i=0; i<paths.Length; i++) {
			if(path == paths[i]) {
				path.SetActive(!value);
				return;
			}
		}
		Debug.Log ("Path does not exists");
	}

	public GameObject newPath() {
		ArrayList indexes = new ArrayList ();
		for (int i=0; i<paths.Length; i++) {
			indexes.Add(i);
		}

		do {
			int index = (int) indexes[Random.Range(0,indexes.Count)];
			indexes.Remove(index);

			// Returns a path if it is not occupied
			if(paths[index].activeSelf) {
                paths[index].SetActive(false);
				return paths[index];
			}
		} while(indexes.Count>0);

		// All path is occupied
		return null;
	}

    public GameObject[] getPoints(GameObject path) {
        GameObject[] points = new GameObject[path.transform.childCount];
        for (int i = 0; i < points.Length; i++) {
            points[i] = path.transform.GetChild(i).gameObject;
        }
        return points;
    }

    public GameObject getNearestPoint(GameObject agent, GameObject[] points) {
        GameObject nearest = points[0];
        float minDistance = (points[0].transform.position - agent.transform.position).magnitude;

        for (int i = 1; i < points.Length; i++ ) {
            float distance = (points[i].transform.position - agent.transform.position).magnitude;
            if (distance < minDistance) {
                nearest = points[i];
                minDistance = distance;
            }
        }
        return nearest;
    }

    void Start() {
        soldiers = new SoldierAI[NPCNumber];

        for (int i = 0; i < NPCNumber; i++) {
            GameObject instance = Instantiate(NPCPrefab) as GameObject;
            soldiers[i] = instance.GetComponent<SoldierAI>();
            // soldiers[i].setDirector(this);
            soldiers[i].name = "Soldier " + (i + 1);

            Character character = instance.GetComponent<Character>();

            Respawner.respawn(character);

            // GameObject path = newPath();
            // Go to random point of path

            // soldiers[i].setPath(path);
        }
    }

    public void ninjaDiscovered(SoldierAI detective, Character ninja) {
        SoldierAI nearestSoldier = soldiers[0], soldier;
        float minDistance = (ninja.transform.position - nearestSoldier.transform.position).magnitude;

        for (int i = 1; i < soldiers.Length; i++) {
            soldier = soldiers[i];
            float distance = (ninja.transform.position - soldier.transform.position).magnitude;

            if (distance<minDistance && soldier != detective) {
                soldier.ninjaDiscovered(ninja);
            }
        }

        nearestSoldier.ninjaDiscovered(ninja);
    }
}
