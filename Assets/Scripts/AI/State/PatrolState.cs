﻿using UnityEngine;
using System.Collections;

public class PatrolState : State {
	public NinjaDiscover ninjaDiscover;
	public LookAround lookAround;
	public PathMovement pathMovement;

    private GameObject path;

    private SoldierAIDirector director;

	void Awake() {
		OnExit ();
	}

	void Start() {
        SoldierAI stateMachine = (SoldierAI)getFSM();
        director = stateMachine.getDirector();
	}

	public override void Action() {

	}

	public override void OnEnter() {
		lookAround.enabled = true;
		pathMovement.enabled = true;
		// pathMovement.setPath (path);
	}
	public override void OnExit() {
        if(path!=null ) director.setPathOccipied(path, false);

		lookAround.enabled = false;
		pathMovement.enabled = false;
	}

    public void setPath(GameObject path) {
        this.path = path;
        pathMovement.setPath(path);
    }

    public GameObject getPath() {
        return path;
    }
}
