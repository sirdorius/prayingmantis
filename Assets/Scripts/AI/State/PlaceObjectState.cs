﻿using UnityEngine;
using System.Collections;

public class PlaceObjectState : State {
    public PlaceAbility turret;
    public float threshold;

    private Character character;
    private Vector3 placePoint;
    private string nextState;

    // Use this for initialization
    void Start() {
        character = GetComponent<Character>();
    }

    public void setPlacePoint(Vector3 point, string state) {
        placePoint = point;
        nextState = state;
        setFSMState("place_object");
    }

    public void setPlacePoint(Vector3 point) {
        setPlacePoint(point, null);
    }

    public override void Action() {
        float distance = (placePoint - transform.position).magnitude;
        if (distance < threshold) {
            turret.activate(character);

            SoldierAI soldier = getFSM() as SoldierAI;
            soldier.newPath();
        }
        else {
            character.goTo(placePoint);
        }
    }

    public override void OnEnter() {
    }

    public override void OnExit() {
    }
    /*
    public override bool isTriggered() {
        if(getFSM().getState() is Pursue) {
            return false;
        }

        return true;
    } */
}