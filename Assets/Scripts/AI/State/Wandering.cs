using UnityEngine;
using System.Collections;

public class Wandering : State {
	public float minDistance;
	public float distance;
	public Character character;
	public float threshold = 2f;
    public float time = 5f;

	private Vector3 target;
    private LookAround lookAround;

	void Start() {
		target = getRandomPosition();
        lookAround = GetComponent<LookAround>();
        OnExit();
	}

	// Update is called once per frame
	public override void Action () {
		Vector3 distance_v = target-character.transform.position;
		float distance = distance_v.magnitude;

		Debug.DrawLine( character.transform.position
		               , target
		               , Color.red);
		Debug.DrawLine( character.transform.position
		               , character.transform.position + distance_v.normalized * minDistance
		               , Color.white);

		bool hit = Physics.Raycast (character.transform.position, distance_v, minDistance);

		if (hit || distance < threshold) {
			target = getRandomPosition();
		} else {
			character.goTo (target);
		}
	}

	public Vector3 getRandomPosition () {
		float radiant = Random.value * Mathf.PI*6/4 + Mathf.PI/2;
		Vector3 vector = Vector3.right * Mathf.Cos (radiant) +
						Vector3.forward * Mathf.Sin (radiant);

		return character.transform.position + vector * distance;
	}

    public override void OnEnter() {
        Invoke("changeState", time);
        lookAround.enabled = true;
    }

    void changeState() {
        SoldierAI soldier = (SoldierAI) getFSM();
        soldier.placeObject(transform.position);
    }

    public override void OnExit() {
        CancelInvoke("changeState");
        lookAround.enabled = false;
    }
}
