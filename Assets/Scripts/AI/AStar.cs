﻿using UnityEngine;
using System.Collections.Generic;
using System.Diagnostics;
using System;

public class AStar : MonoBehaviour {
	
	public GameObject beginning;
	
	public GameObject ending;
	public GameObject start;
    public GameObject end;
	private List<Vector3> path = new List<Vector3>();
	public LayerMask masks;

	private static Graph graph;
	private List<Node> notGraph = new List<Node>();
	public float NODE_DISTANCE = 5f;
	public float OBSTACLE_DETECTION_RADIUS = 0.5f;
	public float OBST_MAX_DISTANCE = 1f;
    public float START_RAYCAST = 10f;
    public float RAYCAST_LENGHT = 11f;
    private Transform shape;
    public Material mat;
	void Awake() {
		masks = masks.Inverse();
		UnityEngine.Debug.Log(masks.MaskToString());
		graph = new Graph(NODE_DISTANCE, OBSTACLE_DETECTION_RADIUS, masks);
		initializeGrid();
	}

	private void initializeGrid() {
		float x = 0f;
		float z = 0f;

		Vector3 pos = new Vector3(beginning.transform.position.x, -0.5f, beginning.transform.position.z);
		Stopwatch stopwatch = new Stopwatch();
		
		stopwatch.Start();
		while(pos.x <= ending.transform.position.x && pos.z <= ending.transform.position.z) {
            RaycastHit hit;
			if(Physics.Raycast(pos + Vector3.up * START_RAYCAST, Vector3.down, out hit, RAYCAST_LENGHT, masks)) {
			} else {
				if (OBST_MAX_DISTANCE > 0) {
					if(!Physics.CheckSphere(pos, OBST_MAX_DISTANCE, masks)) {
						graph.addNode(new Node(pos));
					} else {
						notGraph.Add(new Node(pos));
					}
					
				}
			}

            //mi sposto sul prossimo nodo da checkare
			if (pos.x + NODE_DISTANCE >= ending.transform.position.x) {
				x = beginning.transform.position.x;
				z = pos.z + NODE_DISTANCE;
			} else {
				x = pos.x + NODE_DISTANCE;
				z = pos.z;
			}

			pos = new Vector3(x, 0.5f, z);
		}


		graph.setNeighboors();

		stopwatch.Stop();
		
		// Get the elapsed time as a TimeSpan value.
		TimeSpan ts = stopwatch.Elapsed;
		
		// Format and display the TimeSpan value. 
		string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
		                                   ts.Hours, ts.Minutes, ts.Seconds,
		                                   ts.Milliseconds / 10);
		UnityEngine.Debug.Log("RunTime " + elapsedTime);

		path = graph.Astar(start.transform.position, end.transform.position);
		UnityEngine.Debug.Log(path.Count);
	}

    void Update() {
        graph.drawNodes();
		/*
        foreach(Node n in notGraph) {
			UnityEngine.Debug.DrawLine(n.getPosition(), n.getPosition() + Vector3.up * 6f, Color.red);
		}
         */
		foreach(Vector3 v in path) {
			UnityEngine.Debug.DrawLine(v, v + Vector3.up * 6f, Color.green);
		}
        //RenderVolume(start.transform.position, end.transform.position, 0.2f, Vector3.up, 0.1f);
        
    }

    public static List<Vector3> setDestination(Vector3 from, Vector3 to) {
        return graph.Astar(from, to);
    }

    private void RenderVolume(Vector3 p1, Vector3 p2, float radius, Vector3 dir, float distance) {

         // if shape doesn't exist yet, create it
        shape = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
        Destroy(shape.collider); // no collider, please!
        shape.renderer.material = mat; // assign the selected material to it

        Vector3 scale; // calculate desired scale
        float diam = 2 * radius; // calculate capsule diameter
        scale.x = diam; // width = capsule diameter
        scale.y = Vector3.Distance(p2, p1) + diam; // capsule height
        scale.z = distance + diam; // volume length
        shape.localScale = scale; // set the rectangular volume size
        // set volume position and rotation
        shape.position = (p1 + p2 + dir.normalized * distance) / 2;
        shape.rotation = Quaternion.LookRotation(dir, p2 - p1);
        shape.renderer.enabled = true; // show it
    }
}
