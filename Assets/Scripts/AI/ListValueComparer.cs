﻿using UnityEngine;
using System.Collections.Generic;

public class ListValueComparer : IComparer<Node> {
	
	public int Compare (Node x, Node y)
	{
		return x.Value.CompareTo(y.Value);
	}
}
