﻿using UnityEngine;
using System.Collections.Generic;

public static class TellMeWalkable {

	public static IList<Node> walkableNeighboors(Node n) {
		List<Node> walkable = new List<Node>();
		List<Node> neighboors = n.getNeighboors();

		foreach (Node neighboor in neighboors) {
			RaycastHit hit;
			if (Physics.Linecast(n.getPosition(), neighboor.getPosition(), out hit)) {

			} else {
				walkable.Add(neighboor);
			}

		}

		return walkable;

	}

}
