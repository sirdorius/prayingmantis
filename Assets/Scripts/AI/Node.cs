﻿using UnityEngine;
using System.Collections.Generic;
using System;


public class Node : IComparable<Node>, IComparer<Node> {
	
	private Vector3 position;

	private List<Node> neighboors = new List<Node>();

	private Node parentNode = null;

	private float value;

	public float Value {
		get {return value;}
		internal set {this.value = value;}
	}

	public Node(Vector3 pos) {
		position = pos;
	}

	public Node() {}

	public Node ParentNode {
		get { return parentNode; }
		internal set {parentNode = value;}
	}
	public void setPosition(Vector3 newPosition) {
		position = newPosition;
	}
	
	public Vector3 getPosition() {
		return position;
	}

	public List<Node> getNeighboors() {
		return neighboors;
	}

	public void addNeighboor(Node n) {
		neighboors.Add(n);
	}

	/*public Node getNeighboor(string pos) {
		if (neighboors.ContainsKey(pos)) {
			return neighboors[pos];
		} else {
			return null;
		}
	}*/

	public float getCoordinateDifference(Node n, int axis) {
		if (axis == 0) {
			return this.position.x - n.getPosition().x;
		} else {
			return this.position.z - n.getPosition().z;
		}
	}

	public float getDistance(Node value)
	{
		if (value == null) return float.PositiveInfinity;
		return Mathf.Pow((this.position.x - value.position.x), 2f) + Mathf.Pow((this.position.z - value.position.z), 2f);
	}

	public float getDistance(Vector3 position) {
		return Mathf.Pow((this.position.x - position.x), 2f) + Mathf.Pow((this.position.z - position.z), 2f);
	}

	public override string ToString() {
		return "Node:"+position.ToString();
	}

	public int CompareTo (Node other)
	{
		Vector3 otherPosition = other.getPosition();
		if (this.position.x == otherPosition.x && this.position.z == otherPosition.z) return 0;
		return 1;

	}

	public int Compare (Node x, Node y)
	{
		Vector3 firstPosition = x.getPosition();
		Vector3 secondPosition = y.getPosition();
		if (firstPosition.x == secondPosition.x && secondPosition.z == firstPosition.z) return 0;
		return 1;
	}
}
