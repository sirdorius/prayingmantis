﻿using UnityEngine;
using System.Collections.Generic;

public class ZComparer : IComparer<Node> {

	public int Compare (Node x, Node y)
	{
		return x.getPosition().z.CompareTo(y.getPosition().z);
	}
}
