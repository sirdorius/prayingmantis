﻿using UnityEngine;
using System.Collections.Generic;

public class XComparer : IComparer<Node> {

	public int Compare (Node x, Node y)
	{
		return x.getPosition().x.CompareTo(y.getPosition().x);
	}
}
