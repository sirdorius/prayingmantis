using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Soldier : Character, NinjaDiscoverObserver {
	public Weapon weapon;
	public NinjaDiscover ninjaDiscover;
	private VisionCone visioncone;
	private Collider smoke = null;

	override protected void Start () {
		base.Start();
		visioncone = transform.FindChild("VisionCone").GetComponent<VisionCone>();
		ninjaDiscover.AddObserver (this);
	}

	public override void OnConnect() {
		base.OnConnect ();
	}

	// Update is called once per frame
	override protected void Update () {
		base.Update();
		
		visioncone.enabled = !smoke;
	}
	public void OnDiscoverEnter(Character c) {
	}
	
	public void OnDiscoverStay(Character c) {
	}
	public void OnDiscoverExit(Character c) {
	}
	
	override protected void ControlPolicy () {
		base.ControlPolicy();
		if (Input.GetKeyDown(KeyCode.R)) {
			weapon.reload();
			anim.SetBool("Reload", true);
			StartCoroutine(stopAnim("Reload"));
		}
		else if (Input.GetMouseButton(0))
			weapon.shoot();

		if (HUD)
			HUD.setOtherInfo ("" + weapon.bulletsRemaning ());
		else {
			Debug.LogWarning(name+" HUD has a null reference");
		}
	}
	
	public void OnTriggerEnter(Collider coll) {
		if (coll.tag == "Smoke") {
			smoke = coll;
			visioncone.clear();
			visioncone.enabled = false;
		}
	}
	
	public void OnTriggerExit(Collider coll) {
		if (coll.tag == "Smoke") {
			visioncone.enabled = true;
		}
	}
	
	public void killed() {
		anim.SetBool("Death", true);
		StartCoroutine(stopAnim("Death"));
		transform.FindChild("DeathBlood").particleSystem.Emit(50);
		transform.FindChild ("VisionCone").GetComponent<VisionCone>().enabled = false;
		transform.FindChild ("VisionCone").renderer.enabled = false;
		collider.enabled = false;
		gameObject.GetComponent<Soldier>().enabled = false;
		StartCoroutine(killedCont());
	}
	
	public IEnumerator killedCont() {
		yield return new WaitForSeconds(timeToRespawn);
		anim.SetBool("Respawn", true);
		StartCoroutine(stopAnim("Respawn"));
		
		transform.FindChild ("VisionCone").GetComponent<VisionCone>().enabled = true;
		transform.FindChild ("VisionCone").renderer.enabled = true;
		collider.enabled = true;
		base.killed ();
	}
}
