﻿using UnityEngine;
using System.Collections;

public class Trail : MonoBehaviour {
	public GameObject prefab;
	public float time = 0.1f;
	public float duration = 10f;
	private float timeTotalElapsed = 0f;
	private float timeElapsed = 0f;

	void Start() {
		if (prefab == null) {
			prefab = Resources.Load("Prefabs/Mark") as GameObject;
		}
	}

	// Use this for initialization
	void Update () {
		if (!gameObject.networkView.isMine && !enabled)
						return;
		if (timeTotalElapsed == 0) {
			timeElapsed = Time.time;
			timeTotalElapsed = Time.time;
		}

		if (Time.time - timeTotalElapsed <= duration) {
			if(Time.time - timeElapsed>time) {
				Network.Instantiate (prefab,
				                     transform.position,
				                     transform.rotation,
				                     0);
				timeElapsed = Time.time;
			}
		} else {
			Destroy(gameObject);
		}



		Invoke ("GetDown", time);

	}
}
