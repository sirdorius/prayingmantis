using UnityEngine;
using System.Collections;

public  abstract class Timer : MonoBehaviour {
	public float time;
	protected float countdown;
	protected bool pause;

	// Use this for initialization
	protected virtual void Start () {
		countdown = time;
		pause = true;
	}
	
	// Update is called once per frame
	protected virtual void Update () {
		if(!pause) {
			if(countdown>0){
				countdown -= Time.deltaTime;
				if(countdown < 0) countdown = 0;
			} else {
				Pause();
				timeout();
			}
		}
	}
	
	public void Pause() {
		pause = true;
	}
	
	public void Restart() {
		pause = false;
	}
	
	public void Reset() {
		Pause();
		countdown = time;
	}
	
	public float getTimeNumber () {
		return countdown;
	}
	
	public string getTime() {
		int time = (int) (countdown *1000);
		int millisecond = time%1000 ;
		int second = time/1000;
		return string.Format("{0:00}:{1:00}",second,millisecond);
	}
	
	public abstract void timeout();
}
