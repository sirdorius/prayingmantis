using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {
	public Rigidbody bullet;
	
	public int bullets;
	private int bulletCount;
	
	public int bulletsInATime = 1;
	public float deltaAccuracyCone = 66f;
	
	public float reloadTime = 1f;
	public float timeBetweenShoots = 0.5f;
	public int damage;
	public float bulletSpeed;
	public float range;
	
	private MyGUI HUD;
	
	private float waiting;
	
	// Use this for initialization
	IEnumerator Start () {
		setBullet(bullets);

		while(true) {
			if(waiting != 0f) {
        		yield return StartCoroutine("wait");
			}
			yield return null;
		}
	}
	
	private void setBullet(int bullets) {
		bulletCount = bullets;
	}
	
	public int bulletsRemaning() {
		return bulletCount;
	}
	
	IEnumerator wait() {
		yield return new WaitForSeconds(waiting);
		if(waiting == reloadTime) setBullet(bullets);
		waiting = 0f;
	}
	
	
	
	public void reload() {
		waiting = reloadTime;
	}
	
	public void shoot() {
		if(waiting==0f && bulletCount >= bulletsInATime) {
			waiting = timeBetweenShoots;
			setBullet(bulletCount-bulletsInATime);
			
			if(bulletsInATime == 1) {
				Network.Instantiate(bullet, transform.position, transform.rotation, 0);
			}
			else {
				Quaternion rot = Quaternion.Euler(transform.eulerAngles.x,
						transform.eulerAngles.y-deltaAccuracyCone/2,
						transform.eulerAngles.z);
				
				float deltaAngle = deltaAccuracyCone/(bulletsInATime-1);
				
				Bullet b;
				for(int i=0; i<bulletsInATime; i++) {
					GameObject o= Network.Instantiate(bullet, transform.position, rot, 0) as GameObject;
					rot = Quaternion.Euler(rot.eulerAngles.x,
						rot.eulerAngles.y+deltaAngle,
						rot.eulerAngles.z );
					if(o!=null){ b = o.GetComponent<Bullet>();
					b.range = range;
					b.damage = damage;
					b.bulletSpeed = bulletSpeed;
					}
				}
			}
		}
	}
	
}


