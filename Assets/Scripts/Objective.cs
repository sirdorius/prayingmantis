using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface ObjectiveObserver {
    void ObjectiveCaught(Objective objective);
}

public class Objective : MonoBehaviour {
	public float blinkFrequency = 10;
    
    private List<ObjectiveObserver> observers;
	
	// Update is called once per frame
	void Update () {
		var c = transform.GetChild(0).renderer.material.color;
		Color newColor = new Color(c.r, c.g, c.b, Mathf.Sin(Time.time*blinkFrequency)/3 + 0.5f);
		foreach (Transform i in transform) {
			i.renderer.material.color = newColor;
		}
	}

    public void AddObserver(ObjectiveObserver observer) {
        if (observers == null) observers = new List<ObjectiveObserver>();
        observers.Add(observer);
    }

    public void taken() {
        if (observers == null) return;

        foreach (ObjectiveObserver o in observers)
            o.ObjectiveCaught(this);
    }
}
