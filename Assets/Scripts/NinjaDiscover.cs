﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public interface NinjaDiscoverObserver {
	void OnDiscoverEnter(Character c);
	void OnDiscoverStay(Character c);
	void OnDiscoverExit(Character c);
}

public class NinjaDiscover : MonoBehaviour {
	public VisionCone visioncone;
	private List<NinjaDiscoverObserver> observers;
	private List<Character> ninjas;

	public void Awake() {
		observers = new List<NinjaDiscoverObserver> ();
		ninjas = new List<Character> ();
	}

	// Update is called once per frame
	public void Update () {
		if (visioncone.enabled) {
			List<Collider> collidersInsideCone = visioncone.collidersInsideCone;

			for(int i=0; i<ninjas.Count; i++) {
				Character ninja = ninjas[i];

				if(ninja==null || !visioncone.collidersInsideCone.Contains(ninja.collider)) {
					foreach(NinjaDiscoverObserver observer in observers) {
						observer.OnDiscoverExit(ninja);
					}
					ninjas.Remove(ninja);
					if(ninja!=null) ninja.setVisible(false);
					i--;
				}
			}

			foreach(Collider coll in collidersInsideCone) {
				if(coll.tag == "Ninja") {
					Character c = coll.GetComponent<Character>();

					if(!ninjas.Contains(c)) {
						ninjas.Add(c);
						c.setVisible(true);
						foreach(NinjaDiscoverObserver observer in observers) {
							observer.OnDiscoverEnter(c);
						}
					}
					else {
						foreach(NinjaDiscoverObserver observer in observers) {
							observer.OnDiscoverStay(c);
						}
					}
				}
			}
		}
	}

	public void AddObserver(NinjaDiscoverObserver observer) {
		observers.Add(observer);
	}

	public void setVisionCone(VisionCone v) {
		visioncone = v;
	}
}
