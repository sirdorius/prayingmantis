﻿using UnityEngine;
using System.Collections;

public class Player {
	public static Character instance;

	public static bool isNinja {
		get {
			return instance.GetComponent<Ninja> () != null;
		}
	}
}
