using UnityEngine;
using System.Collections;

public class NetworkManager : MonoBehaviour {
	
	public string serverIP = "localhost";
	public int serverPort = 345678;
	public int maxConnections = 3;
	
	public GameObject playerPrefab;

	void OnGUI() {
		if (!Network.isClient && !Network.isServer)
			MenuGUI();
	}
	
	void MenuGUI() {
		if (GUILayout.Button("Start Server")) {
			Network.InitializeServer(maxConnections, serverPort, false);
		}
		
		if (GUILayout.Button("Connect to server")) {
			Network.Connect(serverIP, serverPort);
		}
	}
		
	void OnConnectedToServer() {
		InitPlayer();
	}
	
	void InitPlayer() {
		Network.Instantiate(playerPrefab, Vector3.zero, Quaternion.identity, 0);
	}
}
