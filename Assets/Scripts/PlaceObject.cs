﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(NetworkView))]

public class PlaceObject : MonoBehaviour {
	private PlaceAbility placer;

	public void setPlacer(PlaceAbility p) {
		placer = p;
	}
	
	void OnDestroy() {
		if(networkView.isMine) placer.removeInstance (gameObject);
	}
}
