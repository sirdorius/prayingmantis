using UnityEngine;
using System.Collections;

public class MoveInDirection : MonoBehaviour {
	public Vector3 dir;
	public float speed;
	
	// Update is called once per frame
	void Update () {
		transform.position += dir*speed*Time.deltaTime;
	}
}
