using UnityEngine;
using System.Collections;

[RequireComponent(typeof(LineRenderer))]

public class Cooldown : MonoBehaviour {
	public int circleDefinition = 100;
	public LineRenderer lineRender;
	public float radius = 5;
	private float cooldown;
	private Vector3[] circlePoints;
	private int prevNum;

	public Transform targetTransform;

	// Use this for initialization
	void Start () {
		circlePoints = new Vector3[circleDefinition];
		for(int i = 0; i<circleDefinition; i++){
			circlePoints[i].x = transform.position.x + radius * Mathf.Cos(2*Mathf.PI*(i/(float) circleDefinition-1));
			circlePoints[i].y = transform.position.y + radius * Mathf.Sin(2*Mathf.PI*(i/(float) circleDefinition-1));
			circlePoints[i].z = transform.position.z;
		}

		cooltimeUpdate (1);
	}

	public void cooltimeUpdate (float time) {
		int num = (int)(circleDefinition*time/cooldown);
		
		if(num>0 && num!=prevNum){
			lineRender.SetVertexCount(num);
			for(int i = 0; i<num; i++){
				if(num<circleDefinition)
					lineRender.SetPosition(i, circlePoints[i]);
			}
		}
		prevNum = num;
	}
	
	public void setCooldown(float c) {
		cooldown = c;
	}
}
