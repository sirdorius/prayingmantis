using UnityEngine;
using System.Collections;

public abstract class ButtonBehaviour : MonoBehaviour {
	bool focused = false;
	private Vector3 scale;
	public int scene;
	Collider coll;
	// Use this for initialization
	protected void Start () {
		print(transform.localScale);
		scale = transform.localScale;
		print (scale);
	}
	
	// Update is called once per frame
	protected void Update () {
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		//Physics.Raycast(ray, out hit, 1000, 1<<8);
   		if (Physics.Raycast (ray, out hit, 1000, 1 << LayerMask.NameToLayer("Default")))
		{
			//print (hit.collider.tag);
			if (hit.collider.tag == "Button" && !focused) {
				focused = true;
				hit.collider.transform.GetComponentInChildren<RotateShuriken>().enabled = true;
				coll = hit.collider;
    			//hit.collider.transform.localScale += new Vector3(1.1F, 1.1F, 1.1F);
			}
			print(hit.collider.tag);
			if (hit.collider.tag != "Button" && focused) {
				coll.transform.GetComponentInChildren<RotateShuriken>().enabled = false;
				//transform.localScale -= new Vector3(1.05F, 1.05F, 1.05F);
				focused = false;
			}
		}
		
		if(Input.GetMouseButtonDown(0) && focused) {
			if(coll.name == "ButtonCredits") {
				print(2);
			} else if(coll.name == "ButtonOnline") {
				Application.LoadLevel(scene);
			} else if(coll.name == "ButtonTraining") {
				print (1);
			} else if(coll.name == "ButtonExit") {
				Application.Quit();
			}
		}
	}
	
	void OnMouseOver() {
		print ("Ciao");
		transform.localScale += new Vector3(1.1F, 1.1F, 1.1F);
	}
}
