using UnityEngine;
using System.Collections;

public class Lobby : MenuState {
	public GameObject soldierPrefab;
	private string playerRole = "Soldier";

	private enum Weapons {RifleAssault, Shotgun};
	private Weapons weapon = Weapons.RifleAssault;
	
	override public void OnGui() {
		/*if(GUILayout.Button("Ninja")) {
			
		} */
		if(GUILayout.Button("Soldier")) {
			playerRole = "Soldier";
		}
		
		GUILayout.BeginHorizontal();
		
		if (playerRole == "Soldier") {
			if(GUILayout.Button("RifleAssault")) {
				weapon = Weapons.RifleAssault;
			}
			else if(GUILayout.Button("Shotgun")) {
				weapon = Weapons.Shotgun;
			}
		}
		
		GUILayout.EndHorizontal();
		
		if(GUILayout.Button("Start Game")) {
			if(playerRole == "Soldier") {
				GameObject soldier = Network.Instantiate(soldierPrefab,
					transform.position,
					Quaternion.identity, 0) as GameObject;
				Soldier character = soldier.GetComponent<Soldier>();
                Respawner.respawn(character);
				
				Weapon w = soldier.GetComponentInChildren<Weapon>();
				
				if(weapon == Weapons.Shotgun) {
					w.bullets = 12;
					w.damage = 40;
					w.bulletsInATime = 6;
					w.timeBetweenShoots = 1f;
					w.reloadTime = 2f;
					w.deltaAccuracyCone = 30f;
					w.bulletSpeed = 100f;
					w.range = 2f;
					w.bulletSpeed = 20f;
				}
			}
			Destroy(gameObject);
		}
	}
}
