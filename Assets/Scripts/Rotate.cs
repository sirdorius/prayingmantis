using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {
	public Vector3 axis;
	public float speed;
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(axis, speed);
	}
}
