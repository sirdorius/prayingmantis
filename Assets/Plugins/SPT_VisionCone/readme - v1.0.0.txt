Spark Plug Tools Dynamic Vision Cone
v1.0.0
February 22, 2012
©2012 Spark Plug Games, LLC. All rights reserved.

URL: http://www.sparkpluggames.com/
Support email: sptools@sparkpluggames.com

----------------------------------------------------------------------------------
 0. Table of Contents
----------------------------------------------------------------------------------

   1. Introduction
   2. Quick Start
   3. Using the Vision Cone
   4. Optimization Hints
   5. Having Fun


----------------------------------------------------------------------------------
 1. Introduction
----------------------------------------------------------------------------------
	The Spark Plug Tools Dynamic Vision Cone plug-in generates a dynamic, customizable
	vision cone that can be resized by arc angle and distance from the origin, 
	and then affected further by dynamic scene colliders.

	The resulting cone can conform to dynamic and static scene geometry by specifing
	a collider mask, and each vision cone in a scene can be set to use different
	parameters for collision tests based on application or gaming need.

	The plug-in also includes a set of editor Gizmo settings that will draw the
	resulting vision cone arc when an object in the hierarchy that contains a
	vision code script is selected.  At run-time, the vision cone will be filled
	in as a solid mesh using the material and optional color specified on the
	GameObject.

----------------------------------------------------------------------------------
 2. Quick Start
----------------------------------------------------------------------------------
	Simply drag and drop the supplied VisionCone prefab object to a GameObject in
	the scene.  By default, the VisionCone will use the Transform of its own
	GameObject as the origin of the arc emitter and direction, but you can specify
	an alternate Transform as the source.  This is handy if you want to use an
	attached look-at controller or have the vision cone follow an entity's eye
	node, for example.

	The supplied Example scene has multiple GameObjects set-up with vision cone
	objects and demonstrate moving objects in the scene affecting the cone.

----------------------------------------------------------------------------------
 3. Using the Vision Cone
----------------------------------------------------------------------------------
	Once a Vision Cone component has been added to a GameObject (or the prefab
	attached to a GameObject hierarchy) it can be modified in the Inspector view
	with the following parameters:

	[Visibility Root] - Sets the root transform that the cone will be projected
		from.  If not set, uses the attached GameObject as the Transform.

	[Visible Distance] - Sets the distance from the [Visibility Root] object that
		the cone will be projected.  Can be adjusted during gameplay by accessing
		the public float value "visibleDistance" in the VisionCone component.

	[Visible Arc] - Sets the arc angle to project to the left and right of the
		forward vector.  For example, if you want the total angle of the vision
		cone projection to be 90 degrees, set the parameter to 45.

	[Inner Arc Angle] - The number of degrees between each segment that is drawn
		within a vision cone.  The smaller the value, the more processing that is
		required but the higher fidelity against dynamic colliders as a result.
		Typical values range from 1.0 to 5.0, in degrees.

	[Collider Mask] - The collision mask that the vision cone should intersect
		with.  If set to Nothing, the vision cone will simple draw an arc and will
		not detect for any geometry in the scene.  If set to Nothing, a warning
		will be issued at runtime, but it can be ignored if the intention to
		draw only an arc was intended.

	[Delayed Update Per Frame] - Optimization setting to specify the delay between
		frames before the dynamic detection is recalculated with geometry in
		the scene.  Typical values are 2-5, but fast moving GameObjects may
		require lower values.  Static entities can have much higher delays from
		10-20.

	[OverrideColor] - Sets the color of the material being used to draw the vision
		cone.  There is a default shader provided which allows for color tinting,
		with the color name "_TintColor" exposed.  This is useful if you want to
		keep the same shader but alternate the color tinting.

	[OverrideColorName] - The name of the color variable in the material shader
		being used to draw the vision cone.  If this is set, the script will try
		to replace the color value in the material with the [OverrideColor] value.

----------------------------------------------------------------------------------
 4. Optimization Hints
----------------------------------------------------------------------------------
	We have had numerous instances of game objects that use the vision cone class
	running on mobile, web, and PC/Mac devices without any impact to performace.
	However, there are always steps that can be taken to minimize the impact to
	the update loop and keep your game or application running at a smooth 60 fps.

	If a game object is not moving often, there is no need to constantly update and
	test the scene for raycasts.  Try adjusting the [DelayFrameUpdate] parameter on
	GameObjects that are not moving fast or are stationary and not interacting with
	a lot of the environment.  Typical values are 2-5, which results in no visible
	loss of accuracy with colliders in the scene.

	The tesselation of the resulting arc can also be affected by increasing or
	decreasing the [InnerArcAngle] parameter.  If you have complicated geometry
	with many small objects that you want to have the vision cone wrap closely,
	try using an [InnerArcAngle] value between 1.0 and 2.0.  You can also
	experiment with the settings in the Inspector view at runtime and customize
	as appropriate to get an accurate looking vision cone based on your scene.

----------------------------------------------------------------------------------
 5. Having Fun!
----------------------------------------------------------------------------------
	We love building games, and we love doing it in Unity to provide a great game
	experience across multiple platforms.  All of our plug-ins have come from
	code we wrote, debugged, refined, tested, debugged more, shipped, and
	packaged up as standalone modules from multiple games we have shipped.

	The following games have been developed, tested, and shipped using this plug-in:

	[Plight of the Zombie]
		iOS Link: https://itunes.apple.com/us/app/plight-of-the-zombie/id541586466?ls=1&mt=8
		Android Link: http://play.google.com/store/apps/details?id=com.sparkpluggames.plightofzombie
		Nook Link: https://nookdeveloper.barnesandnoble.com/tools/dev/linkManager/2940043906670
		PC/Mac Link: http://www.plightofthezombie.com

	[Plight of the Zombie - Free]
		iOS Link: https://itunes.apple.com/us/app/plight-zombie-lite-edition/id521246573?mt=8
		Android Link: http://play.google.com/store/apps/details?id=com.sparkpluggames.plightofzombie
		PC/Mac Link: http://www.plightofthezombie.com

	[Run of the Zombie]
		iOS, Android Link: {pending}
		PC/Mac Link: http://www.runofthezombie.com