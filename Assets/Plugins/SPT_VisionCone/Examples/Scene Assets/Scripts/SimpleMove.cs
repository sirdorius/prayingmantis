//----------------------------------------------
// Spark Plug Tools Vision Cone System
// v1.0.0
// �2013 Spark Plug Games, LLC. All rights reserved.
//----------------------------------------------

using UnityEngine;
using System.Collections;

public class SimpleMove : MonoBehaviour {

	public Transform targetObject;
	public float speed;

	private CharacterController _control = null;

	//------------------------------
	void Start() {
		if (null == targetObject)
			targetObject = gameObject.transform;

		_control = targetObject.gameObject.GetComponent<CharacterController>();

		if (speed <= 0.0f)
			speed = 1.0f;
	}

	//------------------------------
	void Update() {
		float deltaX = Input.GetAxis("Horizontal");
		float deltaZ = Input.GetAxis("Vertical");

		Vector3 pos = targetObject.position;
		if (null == _control) {
			pos.x += deltaX * speed * Time.deltaTime;
			pos.z += deltaZ * speed * Time.deltaTime;
			targetObject.position = pos;
		}
		else {
			_control.Move(new Vector3(deltaX * speed * Time.deltaTime, 0.0f, deltaZ * speed * Time.deltaTime));
		}
	}
}
