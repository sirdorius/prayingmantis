//----------------------------------------------
// Spark Plug Tools Vision Cone System
// v1.0.0
// �2013 Spark Plug Games, LLC. All rights reserved.
//----------------------------------------------

using UnityEngine;
using System.Collections;

/// <summary>
///  Simple script to move an object from a starting point to a destination
///  This is for example purposes only, as there are much better scripts (like iTween) that will do this more efficiently
/// </summary>
public class ObjectPingPong : MonoBehaviour {

	public Transform targetObject;
	public Vector3 offsetPosition;
	public float moveTime;
	public float delayTime;
	public Material altMaterial;

	private float _delay;
	private float _time;
	private float _direction;
	private Vector3 _startPos;
	private Vector3 _endPos;

	//------------------------------
	void Start() {
		if (null == targetObject)
			targetObject = gameObject.transform;

		_startPos = targetObject.transform.position;
		_endPos = _startPos + offsetPosition;
		_time = 0.0f;
		_direction = 1.0f;

		// Make sure our time to move isn't zero or we'll get some bad divide by zero results
		if (moveTime <= 0.0f)
			moveTime = 1.0f;
	}

	//------------------------------
	void Update () {
		//_time + _direction * Time.deltaTime;
		if (_delay > 0.0f) {
			_delay -= Time.deltaTime;

			if (_delay <= 0.0f) {
				ChangeDirection();
			}

			return;
		}

		_time += _direction * Time.deltaTime / moveTime;
		Vector3 pos = Vector3.Lerp(_startPos, _endPos, _time);
		targetObject.position = pos;

		// If lerp target has arrived, start a delay counter
		if (_time < 0.0f || _time > 1.0f ) {
			_delay = delayTime;
		}
	}

	/// <summary>
	/// For demonstration purposes only, change the color of the vision cone when direction of the object changes
	/// </summary>
	void ChangeDirection() {
		_direction = -_direction;

		VisionCone cone = gameObject.GetComponentInChildren<VisionCone>();
		if (null != cone) {
			Color32 direction1Color = new Color32(57, 57, 255, 55);
			Color32 direction2Color = new Color32(255, 57, 57, 55);

			cone.SetMaterial(altMaterial);
			cone.SetTintColor("_TintColor", _direction < 0.0f ? direction1Color : direction2Color);
		}
	}
}
