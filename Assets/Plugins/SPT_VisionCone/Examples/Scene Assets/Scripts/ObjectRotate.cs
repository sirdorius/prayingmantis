//----------------------------------------------
// Spark Plug Tools Vision Cone System
// v1.0.0
// �2013 Spark Plug Games, LLC. All rights reserved.
//----------------------------------------------

using UnityEngine;
using System.Collections;

/// <summary>
///  Simple script to rotate an object along a range and then pause for a set amount of time
///  This is for example purposes only, as there are much better scripts (like iTween) that will do this more efficiently
/// </summary>
public class ObjectRotate : MonoBehaviour {

	public Transform targetObject;
	public float rotateRange;
	public float rotatePerFrame;
	public float delayBetweenRotation;

	private float _delay;
	private float _targetAngle;
	private Quaternion _targetRotation;

	//------------------------------
	void Start() {
		if (null == targetObject)
			targetObject = gameObject.transform;

		_delay = 0.0f;
		_targetAngle = rotateRange;
		ToggleTargetLookAt();
	}

	//------------------------------
	void ToggleTargetLookAt() {
		// Get our current facing direction in Euler angle format
		Vector3 src = targetObject.rotation.eulerAngles;

		// Set our target facing direction
		float destAngle = src.y + _targetAngle;

		// Convert back to Quaternion form
		_targetRotation = Quaternion.Euler(src.x, destAngle, src.z);

		// Flip the direction we're looking at next
		_targetAngle = -_targetAngle;
	}

	//------------------------------
	void Update() {
		if( _delay > 0.0f ) {
			_delay -= Time.deltaTime;

			// Next frame we'll change direction
			if (_delay <= 0.0f) {
				ToggleTargetLookAt();
			}

			return;
		}

		// Rotate at the rate we specified
		targetObject.rotation = Quaternion.RotateTowards(targetObject.rotation, _targetRotation, rotatePerFrame);

		// See if we are at our target, and if so pause for set amount of time
		if (Mathf.Approximately(_targetRotation.eulerAngles.y, targetObject.rotation.eulerAngles.y)) {
			_delay = delayBetweenRotation;
		}
	}
}
