//----------------------------------------------
// Spark Plug Tools Vision Cone System
// v1.0.0
// ©2013 Spark Plug Games, LLC. All rights reserved.
//----------------------------------------------

using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]

public class VisionCone : MonoBehaviour {

	public float visibleDistance;
	public float visibleArc;
	public float innerArcAngle;
	public Transform visibilityRoot;
	public LayerMask collideWith;
	public List<Collider> collidersInsideCone;
	public int delayFrameUpdate;
	public Color overrideColor;
	public string overrideColorName;

	// These lists hold the mesh as it is constructed in the LateUpdate
	private List<Vector3> _vertices = new List<Vector3>();
	private List<Vector2> _uv = new List<Vector2>();
	private List<int> _triangles = new List<int>();

	// Frame delays, if set
	private int _lastFrameUpdate;

	// Cached mesh components
	private Mesh _mesh = null;
	private MeshRenderer _meshRenderer = null;

	//------------------------------
	void Start() {

		if (null == visibilityRoot)
			visibilityRoot = gameObject.transform;

		_mesh = GetComponent<MeshFilter>().mesh;
		_meshRenderer = GetComponent<MeshRenderer>();

		if (Mathf.Approximately(visibleDistance, 0.0f))
			Debug.LogWarning("Vision cone will not be drawn - visibleDistance is 0", gameObject);
		if (Mathf.Approximately(visibleArc, 0.0f))
			Debug.LogWarning("Vision cone will not be drawn - arc is 0", gameObject);
		if (0 == collideWith)
			Debug.LogWarning("Vision cone mask not set, will only draw arc with no collison with geometry in scene", gameObject);

		innerArcAngle = Mathf.Clamp( innerArcAngle, 0.1f, visibleArc );
		
		if( overrideColorName.Length > 0 )
			SetTintColor(overrideColorName, overrideColor);
	}

	//------------------------------
	public bool SetTintColor(string colorName, Color32 newColor) {
		if (null == _meshRenderer) {
			Debug.LogError("Could not find mesh renderer component on vision cone for tint color override");
			return false;
		}

		Material material = _meshRenderer.material;
		material.SetColor(colorName, newColor);

		// Also force a redraw if the cone is not on a refresh loop this frame
		_lastFrameUpdate = 0;

		return true;
	}

	//------------------------------
	/// <summary>
	///  Set alternate material at run-time
	/// </summary>
	public bool SetMaterial(Material altMaterial) {
		if (null == _meshRenderer) {
			Debug.LogError("Could not find mesh renderer component on vision cone for material override");
			return false;
		}

		_meshRenderer.material = altMaterial;

		// Also force a redraw if the cone is not on a refresh loop this frame
		_lastFrameUpdate = 0;

		return true;
	}

	//------------------------------
	void LateUpdate() {
		// If there is a delay frame update, don't update until we pass this frame count
		if (_lastFrameUpdate > 0) {
			_lastFrameUpdate--;
			return;
		}
		else if (delayFrameUpdate > 0) {
			// Set the delay to skip and process the frame normally.  The next iteration will skip the correct number of frames
			_lastFrameUpdate = delayFrameUpdate;
		}

		// Clear out the old cone
		_mesh.Clear();
		_vertices.Clear();
		_uv.Clear();
		_triangles.Clear();

		// Always start with the point of the cone
		_vertices.Add(Vector3.zero);
		_uv.Add(Vector2.zero);

		// Build the rest of the cone a few degrees at a time
		innerArcAngle = Mathf.Clamp(innerArcAngle, 0.1f, visibleArc);
		
		collidersInsideCone = new List<Collider>();
		
		for (float angle = -1.0f * visibleArc; angle <= visibleArc; angle += innerArcAngle) {
			
			Quaternion currRot = Quaternion.identity;
			currRot.eulerAngles = new Vector3(0.0f, angle, 0.0f);

			// Calculate where the cone should reach to at this angle
			Vector3 checkVector = currRot * (visibleDistance * Vector3.forward);

			// Check for obstacle at this angle
			RaycastHit hitInfo;
			if (collideWith != 0 && Physics.Linecast(visibilityRoot.position, visibilityRoot.TransformPoint(checkVector), out hitInfo, collideWith)) {
				if( !collidersInsideCone.Contains( hitInfo.collider ) ) {
					collidersInsideCone.Add(hitInfo.collider);
				}
				// Obstacle found, use the hit location for the cone
				Vector3 hitPos = visibilityRoot.InverseTransformPoint(hitInfo.point);
				_vertices.Add(hitPos);
				float uvFactor = hitPos.magnitude / visibleDistance;
				_uv.Add(new Vector2(uvFactor * (visibleArc + angle) / (innerArcAngle * visibleArc), uvFactor));
			}
			else {
				// No obstacle, use the full visibleDistance of the cone
				_vertices.Add(checkVector);
				_uv.Add(new Vector2((visibleArc + angle) / (innerArcAngle * visibleArc), 1.0f));
			}
		}

		// Create the mesh triangles from the vertices
		for (int i = 2; i < _vertices.Count; i++) {
			_triangles.Add(0);
			_triangles.Add(i - 1);
			_triangles.Add(i);
		}

		// Finally, create the actual mesh from vertices, UVs, and triangles
		_mesh.vertices = _vertices.ToArray();
		_mesh.uv = _uv.ToArray();
		_mesh.triangles = _triangles.ToArray();
	}

	//------------------------------
	/// <summary>
	///  Draw debugging information, only called from in the editor
	/// </summary>
	void OnDrawGizmosSelected () {
		if (Application.isPlaying)
			return;

		Color drawColor = (overrideColorName.Length > 0)?overrideColor:Color.red;
		drawColor.a = 1.0f;
		Gizmos.color = drawColor;

		Transform root = visibilityRoot;
		if (null == root)
			root = gameObject.transform;

		// Forward direction
		Vector3 direction = root.TransformDirection(Vector3.forward) * visibleDistance;
		Gizmos.DrawRay(root.position, direction);

		// Left and right side of the arc extents
		Gizmos.DrawRay(root.position, visibleDistance * (Quaternion.AngleAxis(-visibleArc, Vector3.up) * transform.forward));
		Gizmos.DrawRay(root.position, visibleDistance * (Quaternion.AngleAxis(visibleArc, Vector3.up) * transform.forward));

		// Fill in the arc
		drawColor.a = 0.25f;
		Gizmos.color = drawColor;
		int innerArcAngles = (int)(visibleArc * 2.0f);
		for (float delta = 0.25f; delta < innerArcAngles; delta += 0.25f) {
			Gizmos.DrawRay(root.position, visibleDistance * (Quaternion.AngleAxis(-visibleArc+delta, Vector3.up) * transform.forward));
		}
	}
	
	public void clear() {
		_mesh.Clear();
		_vertices.Clear();
		_uv.Clear();
		_triangles.Clear();
		
	}
}