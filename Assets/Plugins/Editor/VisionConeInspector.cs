//----------------------------------------------
// Spark Plug Tools Vision Cone System
// v1.0.0
// �2013 Spark Plug Games, LLC. All rights reserved.
//----------------------------------------------

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(VisionCone))]
public class VisionConeInspector : Editor {
	/// <summary>
	/// Draw the inspector widget.
	/// </summary>

	public override void OnInspectorGUI() {
		GUILayout.Space(12f);
		VisionCone visionCone = target as VisionCone;
		EditorGUIUtility.LookLikeControls(220.0f, 10.0f);

		float visibleDistance = EditorGUILayout.FloatField("Visible Distance:", visionCone.visibleDistance);
		float visibleArc = EditorGUILayout.FloatField("Visible Arc:", visionCone.visibleArc);
		GUILayout.Space(12f);
		EditorGUIUtility.LookLikeControls(150.0f);
		Transform visibilityRoot = EditorGUILayout.ObjectField("Visibility Root:", visionCone.visibilityRoot, typeof(Transform), true) as Transform;

		GUILayout.BeginHorizontal();
		GUILayout.Label("Collider Mask:", GUILayout.Width(145.0f));
		int collideWith = LayerMaskField(visionCone.collideWith, GUILayout.Width(105f));
		GUILayout.EndHorizontal();

		EditorGUIUtility.LookLikeControls(220.0f, 10.0f);
		GUILayout.Space(12f);
		float innerArcAngle = EditorGUILayout.FloatField("Inner Arc Angle:", visionCone.innerArcAngle);
		int delayFrameUpdate = EditorGUILayout.IntField("Delayed Update Per Frame:", visionCone.delayFrameUpdate);

		EditorGUIUtility.LookLikeControls(150.0f);
		GUILayout.Space(12f);
		Color overrideColor = EditorGUILayout.ColorField("Override Color:", visionCone.overrideColor);
		string overrideColorName = EditorGUILayout.TextField("Shader Color Name:", visionCone.overrideColorName);

		if (visibleDistance != visionCone.visibleDistance) {
			Undo.RegisterUndo(visionCone, "Visible Distance");
			EditorUtility.SetDirty(visionCone);
			visionCone.visibleDistance = visibleDistance;
		}
		if (visibleArc != visionCone.visibleArc) {
			Undo.RegisterUndo(visionCone, "Visible Arc");
			EditorUtility.SetDirty(visionCone);
			visionCone.visibleArc = visibleArc;
		}
		if (visibilityRoot != visionCone.visibilityRoot) {
			Undo.RegisterUndo(visionCone, "Visible Root");
			EditorUtility.SetDirty(visionCone);
			visionCone.visibilityRoot = visibilityRoot;
		}
		if (collideWith != (int)visionCone.collideWith) {
			Undo.RegisterUndo(visionCone, "Visible Arc");
			EditorUtility.SetDirty(visionCone);
			visionCone.collideWith = (LayerMask)collideWith;
		}
		if (innerArcAngle != visionCone.innerArcAngle) {
			Undo.RegisterUndo(visionCone, "Inner Arc Angle");
			EditorUtility.SetDirty(visionCone);
			visionCone.innerArcAngle = innerArcAngle;
		}
		if (delayFrameUpdate != visionCone.delayFrameUpdate) {
			Undo.RegisterUndo(visionCone, "deltaFrameUpdate");
			EditorUtility.SetDirty(visionCone);
			visionCone.delayFrameUpdate = delayFrameUpdate;
		}
		if (overrideColor != visionCone.overrideColor) {
			Undo.RegisterUndo(visionCone, "overrideColor");
			EditorUtility.SetDirty(visionCone);
			visionCone.overrideColor = overrideColor;
		}
		if (overrideColorName != visionCone.overrideColorName) {
			Undo.RegisterUndo(visionCone, "overrideColorName");
			EditorUtility.SetDirty(visionCone);
			visionCone.overrideColorName = overrideColorName;
		}
	}

	/// <summary>
	/// Layer mask field, originally from:
	/// http://answers.unity3d.com/questions/60959/mask-field-in-the-editor.html
	/// </summary>
	public static int LayerMaskField(string label, int mask, params GUILayoutOption[] options) {
		List<string> layers = new List<string>();
		List<int> layerNumbers = new List<int>();

		string selectedLayers = "";

		for (int i = 0; i < 32; ++i) {
			string layerName = LayerMask.LayerToName(i);

			if (!string.IsNullOrEmpty(layerName)) {
				if (mask == (mask | (1 << i))) {
					if (string.IsNullOrEmpty(selectedLayers)) {
						selectedLayers = layerName;
					}
					else {
						selectedLayers = "Mixed";
					}
				}
			}
		}

		if (Event.current.type != EventType.MouseDown && Event.current.type != EventType.ExecuteCommand) {
			if (mask == 0) {
				layers.Add("Nothing");
			}
			else if (mask == -1) {
				layers.Add("Everything");
			}
			else {
				layers.Add(selectedLayers);
			}
			layerNumbers.Add(-1);
		}

		layers.Add((mask == 0 ? "[+] " : "      ") + "Nothing");
		layerNumbers.Add(-2);

		layers.Add((mask == -1 ? "[+] " : "      ") + "Everything");
		layerNumbers.Add(-3);

		for (int i = 0; i < 32; ++i) {
			string layerName = LayerMask.LayerToName(i);

			if (layerName != "") {
				if (mask == (mask | (1 << i))) {
					layers.Add("[+] " + layerName);
				}
				else {
					layers.Add("      " + layerName);
				}
				layerNumbers.Add(i);
			}
		}

		bool preChange = GUI.changed;

		GUI.changed = false;

		int newSelected = 0;

		if (Event.current.type == EventType.MouseDown) {
			newSelected = -1;
		}

		if (string.IsNullOrEmpty(label)) {
			newSelected = EditorGUILayout.Popup(newSelected, layers.ToArray(), EditorStyles.layerMaskField, options);
		}
		else {
			newSelected = EditorGUILayout.Popup(label, newSelected, layers.ToArray(), EditorStyles.layerMaskField, options);
		}

		if (GUI.changed && newSelected >= 0) {
			if (newSelected == 0) {
				mask = 0;
			}
			else if (newSelected == 1) {
				mask = -1;
			}
			else {
				if (mask == (mask | (1 << layerNumbers[newSelected]))) {
					mask &= ~(1 << layerNumbers[newSelected]);
				}
				else {
					mask = mask | (1 << layerNumbers[newSelected]);
				}
			}
		}
		else {
			GUI.changed = preChange;
		}
		return mask;
	}

	public static int LayerMaskField(int mask, params GUILayoutOption[] options) {
		return LayerMaskField(null, mask, options);
	}
}
